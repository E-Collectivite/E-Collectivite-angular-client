import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SubscriptionListComponent} from './subscription-list/subscription-list.component';
import {SubscriptionFormComponent} from './subscription-form/subscription-form.component';
import {SubscriptionComponent} from './subscription/subscription.component';
import {AuthenticationGuard} from '../authentication/guard/authentication.guard';

const routes: Routes = [
  {
    path: 'subscriptions',
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: SubscriptionListComponent
      },
      {
        path: 'add',
        component: SubscriptionFormComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            component: SubscriptionComponent,
          },
          {
            path: 'edit',
            component: SubscriptionFormComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscriptionsRoutingModule {
}
