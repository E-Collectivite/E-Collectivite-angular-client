import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {By} from '@angular/platform-browser';

import { SubscriptionComponent } from './subscription.component';
import {SubscriptionService} from '../shared/subscription.service';
import {SubscriptionServiceMock} from '../shared/subscription.service.mock';

describe('SubscriptionComponent', () => {
  let component: SubscriptionComponent;
  let fixture: ComponentFixture<SubscriptionComponent>;
  const mock = {
    params: Observable.of({id: 1})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionComponent ],
      providers: [
        {provide: SubscriptionService, useClass: SubscriptionServiceMock},
        {provide: ActivatedRoute, useValue: mock}

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should return only one subscription', () => {
    fixture.detectChanges();
    const subscription = fixture.debugElement.queryAll(By.css('ul'));
    expect(subscription.length).toEqual(1);
  });

  it('should match the first subscription test', () => {
    fixture.detectChanges();
    const subscription = fixture.debugElement.queryAll(By.css('ul'));
    const subscriptionTextContent = subscription[0].nativeElement.textContent;
    expect(subscriptionTextContent).toContain(SubscriptionServiceMock.SUBSCRIPTION_1.name);
  });
});
