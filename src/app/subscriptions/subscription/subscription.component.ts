import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subscription} from '../shared/subscription.model';
import {SubscriptionService} from '../shared/subscription.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {
  subscription: Subscription;

  constructor(private subscriptionService: SubscriptionService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.subscriptionService.getSubscription(params['id'])
    ).subscribe(subscription => this.subscription = subscription);
  }

}
