import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {SubscriptionService} from './subscription.service';

describe('SubscriptionService', () => {

  const SUBSCRIPTION_1 = {
    id: 1,
    name: 'EVERYTHING',
  };
  const SUBSCRIPTION_2 = {
    id: 2,
    name: 'SIGNATURE',
  };
  const SUBSCRIPTIONS = [SUBSCRIPTION_1, SUBSCRIPTION_2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SubscriptionService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([SubscriptionService], (service: SubscriptionService) => {
    expect(service).toBeTruthy();
  }));

  describe('getSubscriptions()', () => {

    it('should load all subscriptions', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
      let result;

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Get);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions.json');
        const options = new ResponseOptions({status: 200, body: SUBSCRIPTIONS});
        connection.mockRespond(new Response(options));
      });

      subscriptionService.getSubscriptions().subscribe(subscriptions => {
        result = subscriptions;
      });

      tick();

      expect(result.length).toBe(SUBSCRIPTIONS.length, 'should have expected number of subscriptions');
      expect(result).toBe(SUBSCRIPTIONS, 'should return the subscription list');
    })));

    it('should be OK returning no subscriptions', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        subscriptionService.getSubscriptions().subscribe(subscriptions => {
          result = subscriptions;
        });

        tick();
        expect(result.length).toBe(0, 'should have no subscriptions');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('deleteSubscription()', () => {
    it('should remove a subscription', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions/2.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        subscriptionService.deleteSubscription(SUBSCRIPTION_2.id).subscribe(subscription => {
          result = subscription;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });

  describe('addSubscription()', () => {

    it('should create a subscription', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
        let result;
        const newSubscription = {
          name: 'SIGNATURE',
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions.json');
          const options = new ResponseOptions({status: 201, body: SUBSCRIPTION_2});
          connection.mockRespond(new Response(options));
        });

        subscriptionService.addSubscription(newSubscription).subscribe(subscription => {
          result = subscription;
        });
        tick();

        expect(result).toBe(SUBSCRIPTION_2);
      })));
  });

  describe('getSubscription(id)', () => {
    it('should load a subscription', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions/1.json');
          const options = new ResponseOptions({status: 200, body: SUBSCRIPTION_1});
          connection.mockRespond(new Response(options));
        });

        subscriptionService.getSubscription(1).subscribe(subscription => {
          result = subscription;
        });

        tick();

        expect(result).toBe(SUBSCRIPTION_1, 'should return only one subscription');
      })));
  });

  describe('editSubscription()', () => {

    it('should edit a subscription', inject([SubscriptionService, MockBackend],
      fakeAsync((subscriptionService: SubscriptionService, mockBackend: MockBackend) => {
        let result;
        const updatedSubscription = SUBSCRIPTION_1;
        updatedSubscription.name = 'Test subscription';

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Put);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/subscriptions/1.json');
          const options = new ResponseOptions({status: 200, body: updatedSubscription});
          connection.mockRespond(new Response(options));
        });

        subscriptionService.editSubscription(updatedSubscription).subscribe(subscription => {
          result = subscription;
        });
        tick();

        expect(result).toBe(updatedSubscription);
      })));
  });
});
