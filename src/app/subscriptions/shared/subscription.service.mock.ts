import {Observable} from 'rxjs/Observable';
import {Response, ResponseOptions} from '@angular/http';

import {Subscription} from './subscription.model';

export class SubscriptionServiceMock {
  static SUBSCRIPTION_1 = {
    id: 1,
    name: 'EVERYTHING'
  };
  static SUBSCRIPTION_2 = {
    id: 2,
    name: 'SIGNATURE'
  };

  static subscriptions = [SubscriptionServiceMock.SUBSCRIPTION_1, SubscriptionServiceMock.SUBSCRIPTION_2];

  getSubscriptions(): Observable<{ subscriptions: Subscription[] }> {
    return Observable.of({subscriptions: SubscriptionServiceMock.subscriptions});
  }

  deleteSubscription(id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

  addSubscription(subscription: Subscription): Observable<Subscription> {
    return Observable.of(subscription);
  }

  getSubscription(id): Observable<Subscription> {
    const subscription = SubscriptionServiceMock.subscriptions.find(currentSubscription => currentSubscription.id === id);
    return Observable.of(subscription);
  }

  editSubscription(subscription: Subscription): Observable<Subscription> {
    return Observable.of(subscription);
  }
}
