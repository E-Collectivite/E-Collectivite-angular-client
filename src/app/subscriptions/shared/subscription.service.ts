import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {environment} from '../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {Subscription} from './subscription.model';

@Injectable()
export class SubscriptionService {
  private url = environment.apiUrl + '/subscriptions';

  constructor(private authHttp: AuthHttp) {
  }

  // TODO: expect pagination
  getSubscriptions(): Observable<{ subscriptions: Subscription[] }> {
    return this.authHttp.get(this.url + '.json')
      .map(response => response.json());
  }

  deleteSubscription(id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  addSubscription(subscription: Subscription): Observable<Subscription> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(subscription);

    return this.authHttp.post(this.url + '.json', body, options)
      .map(response => response.json());
  }

  getSubscription(id): Observable<Subscription> {
    return this.authHttp.get(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  editSubscription(subscription: Subscription): Observable<Subscription> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(subscription);

    return this.authHttp.put(this.url + '/' + subscription.id + '.json', body, options)
      .map(response => response.json());
  }
}
