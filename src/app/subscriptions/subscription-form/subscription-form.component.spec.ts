import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {SubscriptionFormComponent} from './subscription-form.component';
import {SubscriptionService} from '../shared/subscription.service';
import {SubscriptionServiceMock} from '../shared/subscription.service.mock';

describe('SubscriptionFormComponent', () => {
  let component: SubscriptionFormComponent;
  let fixture: ComponentFixture<SubscriptionFormComponent>;
  let params: Subject<Params>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };
  beforeEach(async(() => {
    params = new Subject<Params>();
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [SubscriptionFormComponent],
      providers: [
        {provide: SubscriptionService, useClass: SubscriptionServiceMock},
        {provide: Router, useValue: router},
        {provide: ActivatedRoute, useValue: {params: params}},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.subscriptionForm.valid).toBeFalsy();
  });

  it('should be valid when name is set', () => {
    const name = component.subscriptionForm.controls['name'];
    expect(name.valid).toBeFalsy();

    // name is required
    expect(name.errors['required']).toBeTruthy();

    name.setValue('TEST');
    expect(name.valid).toBeTruthy();

    expect(component.subscriptionForm.valid).toBeTruthy();
  });

  it('should add the subscription', () => {
    params.next({});
    expect(component.subscriptionForm.valid).toBeFalsy();
    component.subscriptionForm.controls['name'].setValue('TEST');
    expect(component.subscriptionForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['subscriptions']);
  });

  it('should pre-render the form when updating a subscription', () => {
    params.next({'id': 1});
    fixture.whenStable().then(() => {
      expect(component.subscriptionForm.controls['name'].value).toEqual('EVERYTHING');
    });
  });

  it('should edit an existing subscription and redirect to the subscription list', () => {
    params.next({'id': 1});
    component.subscriptionForm.controls['name'].setValue('Test subscription');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['subscriptions']);
    expect(component.subscription.name).toEqual('Test subscription');
  });
});
