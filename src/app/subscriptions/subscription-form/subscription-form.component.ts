import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {SubscriptionService} from '../shared/subscription.service';
import {Subscription} from '../shared/subscription.model';

@Component({
  selector: 'app-subscription-form',
  templateUrl: './subscription-form.component.html',
  styleUrls: ['./subscription-form.component.css']
})
export class SubscriptionFormComponent implements OnInit {
  subscription_id: number;
  subscription: Subscription = new Subscription();
  subscriptionForm: FormGroup;

  constructor(private subscriptionService: SubscriptionService, private fb: FormBuilder,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.buildForm();

    this.route.params.subscribe(params => {
      if (!params['id']) {
        return;
      }
      this.subscription_id = params['id'];
      this.subscriptionService.getSubscription(this.subscription_id)
        .subscribe(subscription => {
          this.subscription = subscription;
          this.subscriptionForm.patchValue({
            'name': subscription.name
          });
        });
    });
  }

  private buildForm() {
    this.subscriptionForm = this.fb.group({
      'name': [],
    });
  }

  submit() {
    this.subscription = Object.assign(this.subscription, this.subscriptionForm.value);
    if (this.subscription.id) {
      this.subscriptionService.editSubscription(this.subscription).subscribe(
        result => this.router.navigate(['subscriptions']));
    } else {
      this.subscriptionService.addSubscription(this.subscription).subscribe(
        result => this.router.navigate(['subscriptions']));
    }
  }

}
