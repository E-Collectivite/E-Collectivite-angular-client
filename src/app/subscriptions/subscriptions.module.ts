import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {SubscriptionsRoutingModule} from './subscriptions-routing.module';
import {SubscriptionListComponent} from './subscription-list/subscription-list.component';
import {SubscriptionService} from './shared/subscription.service';
import { SubscriptionFormComponent } from './subscription-form/subscription-form.component';
import { SubscriptionComponent } from './subscription/subscription.component';

@NgModule({
  imports: [
    CommonModule,
    SubscriptionsRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SubscriptionListComponent, SubscriptionFormComponent, SubscriptionComponent],
  providers: [SubscriptionService]
})

export class SubscriptionsModule {
}
