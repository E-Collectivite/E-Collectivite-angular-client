import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

import {SubscriptionListComponent} from './subscription-list.component';
import {SubscriptionService} from '../shared/subscription.service';
import {SubscriptionServiceMock} from '../shared/subscription.service.mock';

describe('SubscriptionListComponent', () => {
  let component: SubscriptionListComponent;
  let fixture: ComponentFixture<SubscriptionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [SubscriptionListComponent],
      providers: [
        {provide: SubscriptionService, useClass: SubscriptionServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display subscriptions', () => {
    const subscriptions = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(subscriptions.length).toEqual(2);
  });

  it('should match first test subscription', () => {
    const subscriptions = fixture.debugElement.queryAll(By.css('tbody tr'));
    const subscription = subscriptions[0].nativeElement.textContent;
    expect(subscription).toContain(SubscriptionServiceMock.SUBSCRIPTION_1.name);
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'deleteSubscription').and.callThrough();

    const mockService = fixture.debugElement.injector.get(SubscriptionService);
    spyOn(mockService, 'deleteSubscription').and.callThrough();

    const subscriptions = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = subscriptions[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteSubscription).toHaveBeenCalled();
    expect(mockService.deleteSubscription).toHaveBeenCalled();
  });

  it('should not call the subscription service remove method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'deleteSubscription').and.callThrough();
    const mockService = fixture.debugElement.injector.get(SubscriptionService);
    spyOn(mockService, 'deleteSubscription').and.callThrough();

    const subscriptions = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = subscriptions[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteSubscription).toHaveBeenCalled();
    expect(mockService.deleteSubscription).not.toHaveBeenCalled();
  });
});
