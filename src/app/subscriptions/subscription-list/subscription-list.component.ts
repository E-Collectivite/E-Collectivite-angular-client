import {Component, OnInit} from '@angular/core';

import {SubscriptionService} from '../shared/subscription.service';
import {Subscription} from '../shared/subscription.model';

@Component({
  selector: 'app-subscription-list',
  templateUrl: './subscription-list.component.html',
  styleUrls: ['./subscription-list.component.css']
})
export class SubscriptionListComponent implements OnInit {
  subscriptions: Subscription[];

  constructor(private subscriptionService: SubscriptionService) {
  }

  ngOnInit() {
    // TODO: get pagination
    this.subscriptionService.getSubscriptions()
      .subscribe(subscriptions => this.subscriptions = subscriptions.subscriptions);
  }

  deleteSubscription(subscription: Subscription) {
    if (confirm('Are you sure to remove ' + subscription.name + ' ?')) {
      this.subscriptionService.deleteSubscription(subscription.id)
        .subscribe(response => {
          // Update the current list of subscriptions
          this.subscriptions = this.subscriptions
            .filter(oldSubscription => oldSubscription.id !== subscription.id);
          return this;
        });
    }
  }

}
