export class LocalGovernment {
  id?: number;
  name: string;
  siren: string;
  // TODO: maybe change it to a Subscription
  subscription_id: number;
  // TODO: maybe change it to a LocalGovernment
  pooling_local_governments_id?: number;
}
