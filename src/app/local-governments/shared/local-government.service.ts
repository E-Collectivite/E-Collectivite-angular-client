import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {LocalGovernment} from './local-government.model';

@Injectable()
export class LocalGovernmentService {
  private url = environment.apiUrl + '/local_governments';

  constructor(private authHttp: AuthHttp) {
  }

  // TODO: expect pagination
  getLocalGovernments(): Observable<{ localGovernments: LocalGovernment[] }> {
    return this.authHttp.get(this.url + '.json')
      .map(response => response.json());
  }

  getLocalGovernment(id): Observable<LocalGovernment> {
    return this.authHttp.get(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  deleteLocalGovernment(id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  addLocalGovernment(localGovernment: LocalGovernment): Observable<LocalGovernment> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(localGovernment);

    return this.authHttp.post(this.url + '.json', body, options)
      .map(response => response.json());
  }

  editLocalGovernment(localGovernment: LocalGovernment): Observable<LocalGovernment> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(localGovernment);

    return this.authHttp.put(this.url + '/' + localGovernment.id + '.json', body, options)
      .map(response => response.json());
  }

}
