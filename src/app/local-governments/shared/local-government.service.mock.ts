import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {LocalGovernment} from './local-government.model';
import {Response, ResponseOptions} from '@angular/http';

export class LocalGovernmentServiceMock {
  static LOCAL_GOVERNMENT_1 = {
    id: 1,
    name: 'ADULLACT',
    siren: '443783170',
    subscription_id: 1,
    pooling_local_government_id: null
  };
  static LOCAL_GOVERNMENT_2 = {
    id: 2,
    name: 'COMMUNE DE MONTPELLIER',
    siren: '213401722',
    subscription_id: 1,
    pooling_local_government_id: null
  };
  static local_governments = [LocalGovernmentServiceMock.LOCAL_GOVERNMENT_1, LocalGovernmentServiceMock.LOCAL_GOVERNMENT_2];

  getLocalGovernments(): Observable<{ localGovernments: LocalGovernment[] }> {
    return Observable.of({localGovernments: LocalGovernmentServiceMock.local_governments});
  }

  getLocalGovernment(id): Observable<LocalGovernment> {
    const localGovernment = LocalGovernmentServiceMock.local_governments.find(currentLocalGovernment => currentLocalGovernment.id === id);
    return Observable.of(localGovernment);
  }

  deleteLocalGovernment(id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

  addLocalGovernment(localGovernment: LocalGovernment): Observable<LocalGovernment> {
    return Observable.of(localGovernment);
  }

  editLocalGovernment(localGovernment: LocalGovernment): Observable<LocalGovernment> {
    return Observable.of(localGovernment);
  }
}
