import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {LocalGovernmentService} from './local-government.service';

describe('LocalGovernmentService', () => {
  const LOCAL_GOVERNMENT_1 = {
    id: 1,
    name: 'ADULLACT',
    siren: '443783170',
    subscription_id: 1,
    pooling_local_government_id: null
  };
  const LOCAL_GOVERNMENT_2 = {
    id: 2,
    name: 'COMMUNE DE MONTPELLIER',
    siren: '213401722',
    subscription_id: 1,
    pooling_local_government_id: null
  };
  const LOCAL_GOVERNMENTS = [LOCAL_GOVERNMENT_1, LOCAL_GOVERNMENT_2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalGovernmentService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([LocalGovernmentService], (service: LocalGovernmentService) => {
    expect(service).toBeTruthy();
  }));

  describe('getLocalGovernments()', () => {

    it('should return all local governments', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments.json');
          const options = new ResponseOptions({status: 200, body: LOCAL_GOVERNMENTS});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.getLocalGovernments().subscribe(localGovernments => {
          result = localGovernments;
        });

        tick();
        expect(result.length).toBe(LOCAL_GOVERNMENTS.length, 'should have expected number of local governments');
        expect(result).toBe(LOCAL_GOVERNMENTS, 'should return the local government list');
      })));

    it('should return no local government', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.getLocalGovernments().subscribe(localGovernments => {
          result = localGovernments;
        });

        tick();
        expect(result.length).toBe(0, 'should have no local governments');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('getLocalGovernment(id)', () => {
    it('should load a local government', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1.json');
          const options = new ResponseOptions({status: 200, body: LOCAL_GOVERNMENT_1});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.getLocalGovernment(1).subscribe(localGovernment => {
          result = localGovernment;
        });

        tick();

        expect(result).toBe(LOCAL_GOVERNMENT_1, 'should return only one local government');
      })));
  });

  describe('deleteLocalGovernment(id)', () => {
    it('should delete a local government', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.deleteLocalGovernment(LOCAL_GOVERNMENT_1.id).subscribe(localGovernment => {
          result = localGovernment;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });

  describe('addLocalGovernment()', () => {

    it('should create a local government', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;
        const newLocalGovernment = {
          name: 'ADULLACT',
          siren: '443783170',
          subscription_id: 1
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments.json');
          const options = new ResponseOptions({status: 201, body: LOCAL_GOVERNMENT_1});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.addLocalGovernment(newLocalGovernment).subscribe(localGovernment => {
          result = localGovernment;
        });
        tick();

        expect(result).toBe(LOCAL_GOVERNMENT_1);
      })));
  });

  describe('editLocalGovernment()', () => {

    it('should edit a user', inject([LocalGovernmentService, MockBackend],
      fakeAsync((localGovernmentService: LocalGovernmentService, mockBackend: MockBackend) => {
        let result;
        const updatedLocalGovernment = LOCAL_GOVERNMENT_2;
        updatedLocalGovernment.siren = '000000000';

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Put);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/2.json');
          const options = new ResponseOptions({status: 201, body: updatedLocalGovernment});
          connection.mockRespond(new Response(options));
        });

        localGovernmentService.editLocalGovernment(updatedLocalGovernment).subscribe(localGovernment => {
          result = localGovernment;
        });
        tick();

        expect(result).toBe(updatedLocalGovernment);
      })));
  });
});
