import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocalGovernment} from '../shared/local-government.model';
import {LocalGovernmentService} from '../shared/local-government.service';

@Component({
  selector: 'app-local-government',
  templateUrl: './local-government.component.html',
  styleUrls: ['./local-government.component.css']
})
export class LocalGovernmentComponent implements OnInit {
  localGovernment: LocalGovernment;

  constructor(private route: ActivatedRoute, private localGovernmentService: LocalGovernmentService) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.localGovernmentService.getLocalGovernment(params['id'])
    ).subscribe(localGovernment => this.localGovernment = localGovernment);
  }

}
