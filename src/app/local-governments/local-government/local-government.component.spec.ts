import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {By} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import {LocalGovernmentComponent} from './local-government.component';
import {LocalGovernmentService} from '../shared/local-government.service';
import {LocalGovernmentServiceMock} from '../shared/local-government.service.mock';

describe('LocalGovernmentComponent', () => {
  let component: LocalGovernmentComponent;
  let fixture: ComponentFixture<LocalGovernmentComponent>;
  const mock = {
    params: Observable.of({id: 1}),
    // required: @see https://stackoverflow.com/questions/41245783/angular-testing-router-params-breaks-test-bed
    snapshot: {}
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [LocalGovernmentComponent],
      providers: [
        {provide: LocalGovernmentService, useClass: LocalGovernmentServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalGovernmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should return only one local government', () => {
    fixture.detectChanges();
    const localGovernment = fixture.debugElement.queryAll(By.css('ul'));
    expect(localGovernment.length).toEqual(1);
  });

  it('first local government should match first local government test', () => {
    fixture.detectChanges();
    const localGovernment = fixture.debugElement.queryAll(By.css('ul'));
    const localGovernmentTextContent = localGovernment[0].nativeElement.textContent;
    expect(localGovernmentTextContent).toContain(LocalGovernmentServiceMock.LOCAL_GOVERNMENT_1.name);
    expect(localGovernmentTextContent).toContain(LocalGovernmentServiceMock.LOCAL_GOVERNMENT_1.siren);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
