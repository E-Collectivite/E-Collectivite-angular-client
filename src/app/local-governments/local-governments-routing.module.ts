import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LocalGovernmentListComponent} from './local-government-list/local-government-list.component';
import {LocalGovernmentComponent} from './local-government/local-government.component';
import {LocalGovernmentFormComponent} from './local-government-form/local-government-form.component';
import {AuthenticationGuard} from '../authentication/guard/authentication.guard';

const routes: Routes = [
  {
    path: 'local_governments',
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: LocalGovernmentListComponent,
      },
      {
        path: 'add',
        component: LocalGovernmentFormComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            component: LocalGovernmentComponent,
          },
          {
            path: 'edit',
            component: LocalGovernmentFormComponent
          },
          {
            path: 'members',
            loadChildren: 'app/local-governments/members/members.module#MembersModule',
          }
        ],
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalGovernmentsRoutingModule {
}
