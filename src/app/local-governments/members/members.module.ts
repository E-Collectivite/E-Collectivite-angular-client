import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MembersRoutingModule} from './members-routing.module';
import {MemberListComponent} from './member-list/member-list.component';
import {AddMemberFormComponent} from './add-member-form/add-member-form.component';
import {MemberService} from './shared/member.service';

@NgModule({
  imports: [
    CommonModule,
    MembersRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MemberListComponent, AddMemberFormComponent],
  providers: [MemberService]
})
export class MembersModule {
}
