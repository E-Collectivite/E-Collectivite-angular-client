import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {MemberService} from '../shared/member.service';
import {User} from '../../../users/shared/user.model';
import {UserService} from '../../../users/shared/user.service';

@Component({
  selector: 'app-add-member-form',
  templateUrl: './add-member-form.component.html',
  styleUrls: ['./add-member-form.component.css']
})
export class AddMemberFormComponent implements OnInit {
  memberForm: FormGroup;
  formErrors = {
    'user_id': '',
  };
  validationMessages = {
    'user_id': {
      'required': 'User id is required.',
    }
  };
  users: User[];

  constructor(private memberService: MemberService, private fb: FormBuilder, private route: ActivatedRoute,
              private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.buildForm();
    this.populateUsers();
  }

  private buildForm() {
    this.memberForm = this.fb.group({
      'user_id': ['', [Validators.required]],
      'local_government_administrator': [false],
    });

    this.memberForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    const form = this.memberForm;

    for (const field of Object.keys(this.formErrors)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  submit() {
    const localGovernmentId = this.route.snapshot.parent.params['id'];
    this.memberService.addMembers(localGovernmentId, [this.memberForm.value]).subscribe(
      result => this.router.navigate(['/local_governments', localGovernmentId, 'members']));
  }

  // FIXME: https://gitlab.adullact.net/E-Collectivite/E-Collectivite-server/issues/45
  private populateUsers() {
    // Get all users
    this.userService.getUsers()
      .subscribe(users => {
        // Get all members of the local government
        this.memberService.getMembers(this.route.snapshot.parent.params['id'])
          .subscribe(members => {
            const usersIds = users.users.map(item => item.id);
            const membersIds = members.users.map(item => item.id);

            this.users = usersIds.map((id, index) => {
              if (membersIds.indexOf(id) < 0) {
                return users.users[index];
              }
            }).concat(membersIds.map((id, index) => {
              if (usersIds.indexOf(id) < 0) {
                return members.users[index];
              }
            })).filter(item => item !== undefined);
          });
      });
  }
}
