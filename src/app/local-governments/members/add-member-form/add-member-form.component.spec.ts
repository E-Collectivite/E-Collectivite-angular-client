import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {AddMemberFormComponent} from './add-member-form.component';
import {MemberService} from '../shared/member.service';
import {MemberServiceMock} from '../shared/member.service.mock';
import {UserService} from '../../../users/shared/user.service';
import {UserServiceMock} from '../../../users/shared/user.service.mock';

describe('AddMemberFormComponent', () => {
  let component: AddMemberFormComponent;
  let fixture: ComponentFixture<AddMemberFormComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          id: 1
        }
      }
    }
  };
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [AddMemberFormComponent],
      providers: [
        {provide: MemberService, useClass: MemberServiceMock},
        {provide: UserService, useClass: UserServiceMock},
        {provide: ActivatedRoute, useValue: mock},
        {provide: Router, useValue: router}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMemberFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be invalid when form is empty', () => {
    expect(component.memberForm.valid).toBeFalsy();
  });

  it('should be valid when form is filled', () => {
    const id = component.memberForm.controls['user_id'];
    expect(id.valid).toBeFalsy();

    // User id is required
    expect(id.errors['required']).toBeTruthy();

    id.setValue(1);
    expect(id.valid).toBeTruthy();

    expect(component.memberForm.valid).toBeTruthy();
  });

  it('should display validation errors to the user', async(() => {
    expect(component.memberForm.valid).toBeFalsy();
    component.memberForm.controls['user_id'].markAsDirty();
    component.onValueChanged();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.formErrors['user_id']).toEqual('User id is required. ');
    });
  }));

  it('should create a local government and redirect to the local government list', () => {
    expect(component.memberForm.valid).toBeFalsy();
    component.memberForm.controls['user_id'].setValue(1);
    expect(component.memberForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/local_governments', mock.snapshot.parent.params.id, 'members']);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
