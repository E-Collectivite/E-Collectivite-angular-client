import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {MemberService} from '../shared/member.service';
import {User} from '../../../users/shared/user.model';
import {LocalGovernmentService} from '../../shared/local-government.service';
import {LocalGovernment} from '../../shared/local-government.model';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {
  members: User[];
  localGovernmentId: number;
  localGovernment: LocalGovernment;

  constructor(private memberService: MemberService, private route: ActivatedRoute, private localGovernmentService: LocalGovernmentService) {
  }

  ngOnInit() {
    this.localGovernmentId = this.route.snapshot.parent.params['id'];

    this.memberService.getMembers(this.localGovernmentId)
      .subscribe(members => this.members = members.users);

    this.localGovernmentService.getLocalGovernment(this.localGovernmentId)
      .subscribe(localGovernment =>  this.localGovernment = localGovernment);
  }

  removeMember(member: User) {
    if (confirm('Are you sure to remove ' + member.name + ' ?')) {
      this.memberService.removeMember(this.localGovernmentId, member.id)
        .subscribe(response => {
          // Update the current list of members
          this.members = this.members
            .filter(oldMember => oldMember.id !== member.id);
          return this;
        });
    }
  }

}
