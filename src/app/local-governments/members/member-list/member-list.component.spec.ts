import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {By} from '@angular/platform-browser';

import {MemberListComponent} from './member-list.component';
import {MemberService} from 'app/local-governments/members/shared/member.service';
import {MemberServiceMock} from 'app/local-governments/members/shared/member.service.mock';
import {LocalGovernmentService} from '../../shared/local-government.service';
import {LocalGovernmentServiceMock} from '../../shared/local-government.service.mock';

describe('MemberListComponent', () => {
  let component: MemberListComponent;
  let fixture: ComponentFixture<MemberListComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          id: 1
        }
      }
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [MemberListComponent],
      providers: [
        {provide: MemberService, useClass: MemberServiceMock},
        {provide: LocalGovernmentService, useClass: LocalGovernmentServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display members', () => {
    const members = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(members.length).toEqual(2);
  });

  it('first member should match first test member', () => {
    const members = fixture.debugElement.queryAll(By.css('tbody tr'));
    const member = members[0].nativeElement.textContent;
    expect(member).toContain(MemberServiceMock.USER_1.name);
    expect(member).toContain(MemberServiceMock.USER_1.mail);
    expect(member).toContain(MemberServiceMock.USER_1.firstname);
    expect(member).toContain(MemberServiceMock.USER_1.lastname);
  });

  it('should call the remove method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'removeMember').and.callThrough();

    const mockService = fixture.debugElement.injector.get(MemberService);
    spyOn(mockService, 'removeMember').and.callThrough();

    const members = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = members[0].queryAll(By.css('button'));
    const deleteButton = buttons[0];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.removeMember).toHaveBeenCalled();
    expect(mockService.removeMember).toHaveBeenCalled();
  });

  it('should not call the member service remove method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'removeMember').and.callThrough();
    const mockService = fixture.debugElement.injector.get(MemberService);
    spyOn(mockService, 'removeMember').and.callThrough();

    const members = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = members[0].queryAll(By.css('button'));
    const deleteButton = buttons[0];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.removeMember).toHaveBeenCalled();
    expect(mockService.removeMember).not.toHaveBeenCalled();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
