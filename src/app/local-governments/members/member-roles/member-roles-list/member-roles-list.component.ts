import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {MemberRolesService} from '../shared/member-roles.service';
import {Role} from '../../../../connectors/services/roles/shared/role.model';
import {MemberService} from '../../shared/member.service';
import {User} from '../../../../users/shared/user.model';

@Component({
  selector: 'app-member-roles-list',
  templateUrl: './member-roles-list.component.html',
  styleUrls: ['./member-roles-list.component.css']
})
export class MemberRolesListComponent implements OnInit {
  memberRoles: Role[];
  localGovernmentId: number;
  memberId: number;
  member: User;

  constructor(private memberRolesService: MemberRolesService, private route: ActivatedRoute, private memberService: MemberService) {
  }

  ngOnInit() {
    this.localGovernmentId = this.route.snapshot.parent.params['id'];
    this.memberId = this.route.snapshot.parent.params['member_id'];

    this.memberRolesService.getMemberRoles(this.localGovernmentId, this.memberId)
      .subscribe(memberRoles => this.memberRoles = memberRoles.roles);

    this.memberService.getMember(this.localGovernmentId, this.memberId)
      .subscribe(member => this.member = member);
  }

  removeRole(role: Role) {
    if (confirm('Are you sure to remove ' + role.name + ' ?')) {
      this.memberRolesService.removeMemberRole(this.localGovernmentId, this.memberId, role.id)
        .subscribe(response => {
          // Update the current list of roles
          this.memberRoles = this.memberRoles
            .filter(oldRole => oldRole.id !== role.id);
          return this;
        });
    }
  }

}
