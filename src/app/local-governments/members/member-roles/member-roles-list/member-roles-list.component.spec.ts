import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {By} from '@angular/platform-browser';

import { MemberRolesListComponent } from './member-roles-list.component';
import {MemberRolesService} from '../shared/member-roles.service';
import {MemberRolesServiceMock} from '../shared/member-roles.service.mock';
import {MemberService} from '../../shared/member.service';
import {MemberServiceMock} from '../../shared/member.service.mock';

describe('MemberRolesListComponent', () => {
  let component: MemberRolesListComponent;
  let fixture: ComponentFixture<MemberRolesListComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          id: 1,
          member_id: 2
        }
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ MemberRolesListComponent ],
      providers: [
        {provide: MemberRolesService, useClass: MemberRolesServiceMock},
        {provide: MemberService, useClass: MemberServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberRolesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display roles', () => {
    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(roles.length).toEqual(2);
  });

  it('first role should match first test role', () => {
    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));
    const role = roles[0].nativeElement.textContent;
    expect(role).toContain(MemberRolesServiceMock.ROLE_1.name);
  });

  it('should call the remove method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'removeRole').and.callThrough();

    const mockService = fixture.debugElement.injector.get(MemberRolesService);
    spyOn(mockService, 'removeMemberRole').and.callThrough();

    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = roles[0].queryAll(By.css('button'));
    const deleteButton = buttons[0];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.removeRole).toHaveBeenCalled();
    expect(mockService.removeMemberRole).toHaveBeenCalled();
  });

  it('should not call the member role service remove method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'removeRole').and.callThrough();
    const mockService = fixture.debugElement.injector.get(MemberRolesService);
    spyOn(mockService, 'removeMemberRole').and.callThrough();

    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = roles[0].queryAll(By.css('button'));
    const deleteButton = buttons[0];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.removeRole).toHaveBeenCalled();
    expect(mockService.removeMemberRole).not.toHaveBeenCalled();
  });
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
