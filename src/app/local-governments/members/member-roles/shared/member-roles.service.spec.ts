import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {MemberRolesService} from './member-roles.service';

const ROLE_1 = {
  id: 1,
  name: 'Agent actes',
  service_id: 1
};
const ROLE_2 = {
  id: 2,
  name: 'Agent hélios',
  service_id: 2
};
const ROLES = [ROLE_1, ROLE_2];

describe('MemberRolesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MemberRolesService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([MemberRolesService], (service: MemberRolesService) => {
    expect(service).toBeTruthy();
  }));

  describe('getMemberRoles()', () => {

    it('should load all roles of a member', inject([MemberRolesService, MockBackend],
      fakeAsync((memberRolesService: MemberRolesService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members/2/roles.json');
          const options = new ResponseOptions({status: 200, body: ROLES});
          connection.mockRespond(new Response(options));
        });

        memberRolesService.getMemberRoles(1, 2).subscribe(roles => {
          result = roles;
        });

        tick();

        expect(result.length).toBe(ROLES.length, 'should have expected number of roles');
        expect(result).toBe(ROLES, 'should return the list of roles');
      })));

    it('should be OK returning no roles', inject([MemberRolesService, MockBackend],
      fakeAsync((memberRolesService: MemberRolesService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/2/members/2/roles.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        memberRolesService.getMemberRoles(2, 2).subscribe(members => {
          result = members;
        });

        tick();
        expect(result.length).toBe(0, 'should have no roles');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('removeMemberRole()', () => {
    it('should remove a member from a local government', inject([MemberRolesService, MockBackend],
      fakeAsync((memberRolesService: MemberRolesService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members/2/roles/1.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        memberRolesService.removeMemberRole(1, 2, 1).subscribe(role => {
          result = role;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });

  describe('addRoles()', () => {
    it('should add a role', inject([MemberRolesService, MockBackend],
      fakeAsync((memberRolesService: MemberRolesService, mockBackend: MockBackend) => {
        let result;
        const newRole = {
          role_id: 1,
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members/1/roles.json');
          const options = new ResponseOptions({status: 201, body: ROLES});
          connection.mockRespond(new Response(options));
        });

        memberRolesService.addRoles(1, 1, newRole).subscribe(roles => {
          result = roles;
        });
        tick();

        expect(result[0]).toBe(ROLE_1);
      })));
  });

  describe('getRoles()', () => {

    it('should load all roles available', inject([MemberRolesService, MockBackend],
      fakeAsync((memberRolesService: MemberRolesService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/roles.json');
          const options = new ResponseOptions({status: 200, body: ROLES});
          connection.mockRespond(new Response(options));
        });

        memberRolesService.getRoles(1).subscribe(roles => {
          result = roles;
        });

        tick();

        expect(result.length).toBe(ROLES.length, 'should have expected number of roles');
        expect(result).toBe(ROLES, 'should return the list of roles');
      })));
  });
});
