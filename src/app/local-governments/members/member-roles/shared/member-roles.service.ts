import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {Role} from '../../../../connectors/services/roles/shared/role.model';

@Injectable()
export class MemberRolesService {
  private url = environment.apiUrl + '/local_governments';

  constructor(private authHttp: AuthHttp) {
  }

  // TODO: returns observable of a list of roles
  getMemberRoles(local_government_id, member_id): Observable<{ roles: Role[] }> {
    return this.authHttp.get(this.url + '/' + local_government_id + '/members/' + member_id + '/roles.json')
      .map(response => response.json());
  }

  removeMemberRole(local_government_id, user_id, role_id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + local_government_id + '/members/' + user_id + '/roles/' + role_id + '.json')
      .map(response => response.json());
  }

  addRoles(local_government_id, user_id, roles): Observable<{ roles: Role[] }> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify({'roles': roles});
    return this.authHttp.post(this.url + '/' + local_government_id + '/members/' + user_id + '/roles.json', body, options)
      .map(response => response.json());
  }

  getRoles(local_government_id): Observable<{ roles: Role[] }> {
    return this.authHttp.get(this.url + '/' + local_government_id + '/roles.json')
      .map(response => response.json());
  }
}
