import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {ResponseOptions, Response} from '@angular/http';

import {Role} from '../../../../connectors/services/roles/shared/role.model';

export class MemberRolesServiceMock {
  static ROLE_1 = {
  id: 1,
  name: 'Agent actes',
  service_id: 1
};
  static ROLE_2 = {
  id: 2,
  name: 'Agent hélios',
  service_id: 2
};

  static roles = [MemberRolesServiceMock.ROLE_1, MemberRolesServiceMock.ROLE_2];

  getMemberRoles(local_government_id, member_id): Observable<{ roles: Role[] }> {
    return Observable.of({roles: MemberRolesServiceMock.roles});
  }

  removeMemberRole(local_government_id, user_id, role_id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

  addRoles(local_government_id, user_id, roles): Observable<{ roles: Role[] }> {
    return Observable.of({roles: MemberRolesServiceMock.roles});
  }

  getRoles(local_government_id): Observable<{ roles: Role[] }> {
    return Observable.of({roles: MemberRolesServiceMock.roles});
  }


}
