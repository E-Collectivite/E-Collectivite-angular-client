import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {MemberRolesFormComponent} from './member-roles-form.component';
import {MemberRolesService} from '../shared/member-roles.service';
import {MemberRolesServiceMock} from '../shared/member-roles.service.mock';

describe('MemberRolesFormComponent', () => {
  let component: MemberRolesFormComponent;
  let fixture: ComponentFixture<MemberRolesFormComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          id: 1,
          member_id: 1
        }
      }
    }
  };
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [MemberRolesFormComponent],
      providers: [
        {provide: MemberRolesService, useClass: MemberRolesServiceMock},
        {provide: ActivatedRoute, useValue: mock},
        {provide: Router, useValue: router}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberRolesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be invalid when form is empty', () => {
    expect(component.memberRolesForm.valid).toBeFalsy();
  });

  it('should be valid when role is set', () => {
    const id = component.memberRolesForm.controls['role_id'];
    expect(id.valid).toBeFalsy();

    // Role id is required
    expect(id.errors['required']).toBeTruthy();

    id.setValue(1);
    expect(id.valid).toBeTruthy();

    expect(component.memberRolesForm.valid).toBeTruthy();
  });

  it('should add role to the member and redirect to its role list', () => {
    expect(component.memberRolesForm.valid).toBeFalsy();
    component.memberRolesForm.controls['role_id'].setValue(1);
    expect(component.memberRolesForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/local_governments', mock.snapshot.parent.params.id,
      'members', mock.snapshot.parent.params.member_id, 'roles']);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
