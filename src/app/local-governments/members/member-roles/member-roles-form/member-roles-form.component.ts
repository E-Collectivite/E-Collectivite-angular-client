import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {MemberRolesService} from '../shared/member-roles.service';
import {Role} from '../../../../connectors/services/roles/shared/role.model';

@Component({
  selector: 'app-member-roles-form',
  templateUrl: './member-roles-form.component.html',
  styleUrls: ['./member-roles-form.component.css']
})
export class MemberRolesFormComponent implements OnInit {
  memberRolesForm: FormGroup;
  roles: Role[];

  constructor(private memberRolesService: MemberRolesService, private fb: FormBuilder,
              private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.buildForm();
    this.populateRoles();
  }

  private buildForm() {
    this.memberRolesForm = this.fb.group({
      'role_id': [],
    });
  }

  submit() {
    const localGovernmentId = this.route.snapshot.parent.params['id'];
    const memberId = this.route.snapshot.parent.params['member_id'];
    this.memberRolesService.addRoles(localGovernmentId, memberId, [this.memberRolesForm.value]).subscribe(
      result => this.router.navigate(['/local_governments', localGovernmentId, 'members', memberId, 'roles']));
  }

  private populateRoles() {
    const localGovernmentId = this.route.snapshot.parent.params['id'];
    const memberId = this.route.snapshot.parent.params['member_id'];

    this.memberRolesService.getRoles(localGovernmentId)
      .subscribe(roles => {
        this.memberRolesService.getMemberRoles(localGovernmentId, memberId)
          .subscribe(memberRoles => {
            const usersIds = roles.roles.map(item => item.id);
            const membersIds = memberRoles.roles.map(item => item.id);

            this.roles = usersIds.map((id, index) => {
              if (membersIds.indexOf(id) < 0) {
                return roles.roles[index];
              }
            }).concat(membersIds.map((id, index) => {
              if (usersIds.indexOf(id) < 0) {
                return memberRoles.roles[index];
              }
            })).filter(item => item !== undefined);
          });
      });
  }
}
