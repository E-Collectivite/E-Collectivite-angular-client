import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import { MemberRolesRoutingModule } from './member-roles-routing.module';
import { MemberRolesListComponent } from './member-roles-list/member-roles-list.component';
import {MemberRolesService} from './shared/member-roles.service';
import { MemberRolesFormComponent } from './member-roles-form/member-roles-form.component';

@NgModule({
  imports: [
    CommonModule,
    MemberRolesRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MemberRolesListComponent, MemberRolesFormComponent],
  providers: [MemberRolesService]

})
export class MemberRolesModule { }
