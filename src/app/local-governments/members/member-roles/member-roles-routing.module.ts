import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MemberRolesListComponent} from './member-roles-list/member-roles-list.component';
import {MemberRolesFormComponent} from './member-roles-form/member-roles-form.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MemberRolesListComponent,
      },
      {
        path: 'add',
        component: MemberRolesFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRolesRoutingModule {
}
