import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MemberListComponent} from './member-list/member-list.component';
import {AddMemberFormComponent} from './add-member-form/add-member-form.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MemberListComponent,
      },
      {
        path: 'add',
        component: AddMemberFormComponent
      },
      {
        path: ':member_id',
        children: [
          {
            // TODO: display something else by default
            path: '',
            component: MemberListComponent
          },
          {
            path: 'roles',
            loadChildren: 'app/local-governments/members/member-roles/member-roles.module#MemberRolesModule',
          }
        ]
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule {
}
