import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {environment} from '../../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {User} from '../../../users/shared/user.model';

@Injectable()
export class MemberService {
  private url = environment.apiUrl + '/local_governments';

  constructor(private authHttp: AuthHttp) {
  }

  // TODO: expect pagination
  getMembers(local_government_id): Observable<{ users: User[] }> {
    return this.authHttp.get(this.url + '/' + local_government_id + '/members.json')
      .map(response => response.json());
  }

  getMember(local_government_id, member_id): Observable<User> {
    return this.authHttp.get(this.url + '/' + local_government_id + '/members/' + member_id + '.json')
      .map(response => response.json());
  }

  // TODO: set up a return type (LocalGovernment + array of user)
  addMembers(local_government_id, members) {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify({'users': members});
    return this.authHttp.post(this.url + '/' + local_government_id + '/members.json', body, options)
      .map(response => response.json());
  }

  removeMember(local_government_id, user_id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + local_government_id + '/members/' + user_id + '.json')
      .map(response => response.json());
  }


}
