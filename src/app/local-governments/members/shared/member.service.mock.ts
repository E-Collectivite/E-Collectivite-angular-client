import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Response, ResponseOptions} from '@angular/http';

import {User} from '../../../users/shared/user.model';

export class MemberServiceMock {
  static LOCAL_GOVERNMENT_1 = {
    id: 1,
    name: 'ADULLACT',
    siren: '443783170',
    subscription_id: 1,
    pooling_local_government_id: null
  };
  static USER_1 = {
    id: 1,
    name: 'admin',
    mail: 'admin@example.com',
    firstname: 'admin',
    lastname: 'admin',
    superadmin: true
  };
  static USER_2 = {
    id: 2,
    name: 'mreyrolle',
    mail: 'maxime.reyrolle@example.com',
    firstname: 'Maxime',
    lastname: 'REYROLLE',
    superadmin: false
  };

  static users = [MemberServiceMock.USER_1, MemberServiceMock.USER_2];

  getMembers(local_government_id): Observable<{ users: User[] }> {
    return Observable.of({users: MemberServiceMock.users});
  }
  getMember(local_government_id, member_id): Observable<User> {
    return Observable.of(MemberServiceMock.USER_1);
  }

  addMembers(local_government_id, members) {
    const local_government = MemberServiceMock.LOCAL_GOVERNMENT_1;
    const user = MemberServiceMock.users.find(currentUser => currentUser.id === members[0].user_id);
    (local_government as any).users = [user];

    return Observable.of(local_government);
  }

  removeMember(local_government_id, user_id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

}
