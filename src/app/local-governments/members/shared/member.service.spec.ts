import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {MemberService} from './member.service';

const LOCAL_GOVERNMENT_1 = {
  id: 1,
  name: 'ADULLACT',
  siren: '443783170',
  subscription_id: 1,
  pooling_local_government_id: null
};

const USER_1 = {
  id: 1,
  name: 'admin',
  mail: 'admin@example.com',
  firstname: 'admin',
  lastname: 'admin',
  superadmin: true
};
const USER_2 = {
  id: 2,
  name: 'mreyrolle',
  mail: 'maxime.reyrolle@example.com',
  firstname: 'Maxime',
  lastname: 'REYROLLE',
  superadmin: false
};
const USERS = [USER_1, USER_2];

describe('MemberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MemberService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([MemberService], (service: MemberService) => {
    expect(service).toBeTruthy();
  }));

  describe('getMembers()', () => {

    it('should load all members', inject([MemberService, MockBackend],
      fakeAsync((memberService: MemberService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members.json');
          const options = new ResponseOptions({status: 200, body: USERS});
          connection.mockRespond(new Response(options));
        });

        memberService.getMembers(1).subscribe(members => {
          result = members;
        });

        tick();

        expect(result.length).toBe(USERS.length, 'should have expected number of members');
        expect(result).toBe(USERS, 'should return the member list');
      })));

    it('should be OK returning no members', inject([MemberService, MockBackend],
      fakeAsync((memberService: MemberService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/2/members.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        memberService.getMembers(2).subscribe(members => {
          result = members;
        });

        tick();
        expect(result.length).toBe(0, 'should have no members');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('getMember(id)', () => {
    it('should load a member', inject([MemberService, MockBackend],
      fakeAsync((memberService: MemberService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members/2.json');
          const options = new ResponseOptions({status: 200, body: USER_2});
          connection.mockRespond(new Response(options));
        });

        memberService.getMember(1, 2).subscribe(member => {
          result = member;
        });

        tick();

        expect(result).toBe(USER_2, 'should return a member');
      })));
  });

  describe('addMembers()', () => {

    it('should add a member', inject([MemberService, MockBackend], fakeAsync((memberService: MemberService, mockBackend: MockBackend) => {
      let result;
      const newMember = {
        user_id: 1,
        local_government_administrator: false,
      };
      // FIXME: one-off assignment
      (LOCAL_GOVERNMENT_1 as any).users = [USER_1];

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Post);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members.json');
        const options = new ResponseOptions({status: 201, body: LOCAL_GOVERNMENT_1});
        connection.mockRespond(new Response(options));
      });

      memberService.addMembers(1, newMember).subscribe(user => {
        result = user;
      });
      tick();

      expect(result.users[0]).toBe(USER_1);
    })));

  });

  describe('removeMember()', () => {
    it('should remove a member from a local government', inject([MemberService, MockBackend],
      fakeAsync((memberService: MemberService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/1/members/2.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        memberService.removeMember(LOCAL_GOVERNMENT_1.id, USER_2.id).subscribe(member => {
          result = member;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });
});
