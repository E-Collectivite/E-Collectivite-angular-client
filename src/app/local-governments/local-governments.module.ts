import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {LocalGovernmentsRoutingModule} from './local-governments-routing.module';
import {LocalGovernmentService} from './shared/local-government.service';
import { LocalGovernmentListComponent } from './local-government-list/local-government-list.component';
import { LocalGovernmentComponent } from './local-government/local-government.component';
import { LocalGovernmentFormComponent } from './local-government-form/local-government-form.component';

@NgModule({
  imports: [
    CommonModule,
    LocalGovernmentsRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [LocalGovernmentListComponent, LocalGovernmentComponent, LocalGovernmentFormComponent],
  providers: [LocalGovernmentService]
})

export class LocalGovernmentsModule {
}
