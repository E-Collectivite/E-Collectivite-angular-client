import {Component, OnInit} from '@angular/core';
import {LocalGovernmentService} from '../shared/local-government.service';
import {LocalGovernment} from '../shared/local-government.model';

@Component({
  selector: 'app-local-government-list',
  templateUrl: './local-government-list.component.html',
  styleUrls: ['./local-government-list.component.css']
})
export class LocalGovernmentListComponent implements OnInit {
  localGovernments: any;

  constructor(private localGovernmentService: LocalGovernmentService) {
  }

  ngOnInit() {
    // TODO: get pagination
    this.localGovernmentService.getLocalGovernments()
      .subscribe(localGovernments => this.localGovernments = localGovernments.localGovernments);
  }

  deleteLocalGovernment(localGovernment: LocalGovernment) {
    // @see angular2-modal or angular-confirmation-popover for better integration with angular
    if (confirm('Are you sure to delete ' + localGovernment.name + ' ?')) {
      this.localGovernmentService.deleteLocalGovernment(localGovernment.id)
        .subscribe(response => {
          // Update the current list of local governments without refreshing the page (save 1 request)
          this.localGovernments = this.localGovernments
            .filter(oldLocalGovernments => oldLocalGovernments.id !== localGovernment.id);
          return this;
        });
    }
  }

}
