import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

import {LocalGovernmentListComponent} from './local-government-list.component';
import {LocalGovernmentService} from '../shared/local-government.service';
import {LocalGovernmentServiceMock} from 'app/local-governments/shared/local-government.service.mock';

describe('LocalGovernmentListComponent', () => {
  let component: LocalGovernmentListComponent;
  let fixture: ComponentFixture<LocalGovernmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [LocalGovernmentListComponent],
      providers: [
        {provide: LocalGovernmentService, useClass: LocalGovernmentServiceMock }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalGovernmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display local governments', () => {
    const localGovernments = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(localGovernments.length).toEqual(2);
  });

  it('first local government should match first test local government', () => {
    const localGovernments = fixture.debugElement.queryAll(By.css('tbody tr'));
    const localGovernment = localGovernments[0].nativeElement.textContent;
    expect(localGovernment).toContain(LocalGovernmentServiceMock.LOCAL_GOVERNMENT_1.name);
    expect(localGovernment).toContain(LocalGovernmentServiceMock.LOCAL_GOVERNMENT_1.siren);
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'deleteLocalGovernment').and.callThrough();

    const mockService = fixture.debugElement.injector.get(LocalGovernmentService);
    spyOn(mockService, 'deleteLocalGovernment').and.callThrough();

    const localGovernments = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = localGovernments[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteLocalGovernment).toHaveBeenCalled();
    expect(mockService.deleteLocalGovernment).toHaveBeenCalled();
  });

  it('should not call the local government service delete method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'deleteLocalGovernment').and.callThrough();
    const mockService = fixture.debugElement.injector.get(LocalGovernmentService);
    spyOn(mockService, 'deleteLocalGovernment').and.callThrough();

    const localGovernments = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = localGovernments[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteLocalGovernment).toHaveBeenCalled();
    expect(mockService.deleteLocalGovernment).not.toHaveBeenCalled();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
