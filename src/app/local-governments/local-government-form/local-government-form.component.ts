import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {LocalGovernmentService} from '../shared/local-government.service';
import {LocalGovernment} from '../shared/local-government.model';
import {Subscription} from '../../subscriptions/shared/subscription.model';
import {SubscriptionService} from '../../subscriptions/shared/subscription.service';

@Component({
  selector: 'app-local-government-form',
  templateUrl: './local-government-form.component.html',
  styleUrls: ['./local-government-form.component.css']
})
export class LocalGovernmentFormComponent implements OnInit {
  id = null;
  localGovernment: LocalGovernment = new LocalGovernment();
  localGovernmentForm: FormGroup;
  poolingLocalGovernments: LocalGovernment[];
  subscriptions: Subscription[];
  formErrors = {
    'name': '',
    'siren': '',
    'subscription_id': '',
  };

  validationMessages = {
    'name': {
      'required': 'Name is required.',
    },
    'siren': {
      'required': 'Siren is required.',
      'pattern': 'This pattern is incorrect.'
    },
    'subscription_id': {
      'required': 'Subscription is required.',
    }
  };

  constructor(private localGovernmentService: LocalGovernmentService, private fb: FormBuilder,
              private route: ActivatedRoute, private router: Router, private subscriptionService: SubscriptionService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (!params['id']) {
        return;
      }
      this.id = +params['id'];
      this.localGovernmentService.getLocalGovernment(this.id)
        .subscribe(localGovernment => this.localGovernment = localGovernment);
    });

    this.populatePoolingLocalGovernments();
    this.populateSubscriptions();
    this.buildForm();
  }

  private buildForm() {
    this.localGovernmentForm = this.fb.group({
      'name': [this.localGovernment.name, [Validators.required]],
      'siren': [this.localGovernment.siren, [Validators.required, Validators.pattern('[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{3}')]],
      'subscription_id': [this.localGovernment.subscription_id, [Validators.required]],
      'pooling_local_governments_id': [this.localGovernment.pooling_local_governments_id],
    });
    this.localGovernmentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.localGovernmentForm) {
      return;
    }
    const form = this.localGovernmentForm;

    for (const field of Object.keys(this.formErrors)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  submit() {
    this.localGovernment = Object.assign(this.localGovernment, this.localGovernmentForm.value);
    if (this.localGovernment.id) {
      this.localGovernmentService.editLocalGovernment(this.localGovernment).subscribe(
        result => this.router.navigate(['/local_governments']));
    } else {
      this.localGovernmentService.addLocalGovernment(this.localGovernment).subscribe(
        result => this.router.navigate(['/local_governments']));
    }
  }

  private populatePoolingLocalGovernments() {
    this.localGovernmentService.getLocalGovernments()
      .subscribe(localGovernments => {
        this.poolingLocalGovernments = localGovernments.localGovernments
          .filter(currentLocalGovernment => currentLocalGovernment.id !== this.id);
      });
  }

  private populateSubscriptions() {
    this.subscriptionService.getSubscriptions()
      .subscribe(subscriptions => {
        this.subscriptions = subscriptions.subscriptions;
      });
  }
}
