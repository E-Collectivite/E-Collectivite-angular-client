import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {LocalGovernmentFormComponent} from './local-government-form.component';
import {LocalGovernmentService} from '../shared/local-government.service';
import {LocalGovernmentServiceMock} from '../shared/local-government.service.mock';
import {SubscriptionService} from '../../subscriptions/shared/subscription.service';
import {SubscriptionServiceMock} from '../../subscriptions/shared/subscription.service.mock';

describe('LocalGovernmentFormComponent', () => {
  let component: LocalGovernmentFormComponent;
  let fixture: ComponentFixture<LocalGovernmentFormComponent>;
  let params: Subject<Params>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    params = new Subject<Params>();
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [LocalGovernmentFormComponent],
      providers: [
        {provide: LocalGovernmentService, useClass: LocalGovernmentServiceMock},
        {provide: ActivatedRoute, useValue: {params: params}},
        {provide: Router, useValue: router},
        {provide: SubscriptionService, useClass: SubscriptionServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalGovernmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be invalid when form is empty', () => {
    expect(component.localGovernmentForm.valid).toBeFalsy();
  });


  it('should be valid when form is filled', () => {
    const name = component.localGovernmentForm.controls['name'];
    expect(name.valid).toBeFalsy();

    // Email field is required
    expect(name.errors['required']).toBeTruthy();
    // Set email to random string
    name.setValue('test');
    expect(name.valid).toBeTruthy();
    expect(component.localGovernmentForm.valid).toBeFalsy();

    const siren = component.localGovernmentForm.controls['siren'];
    expect(siren.valid).toBeFalsy();
    expect(siren.errors['pattern']).toBeFalsy();

    // Set random siren
    siren.setValue('1234');
    expect(siren.errors['pattern']).toBeTruthy();

    // Set correct siren
    siren.setValue('000000000');
    expect(siren.valid).toBeTruthy();

    expect(component.localGovernmentForm.valid).toBeFalsy();

    const subscription = component.localGovernmentForm.controls['subscription_id'];
    expect(subscription.valid).toBeFalsy();
    subscription.setValue('1');

    expect(component.localGovernmentForm.valid).toBeTruthy();

  });

  it('should display validation errors to the user', async(() => {
    expect(component.localGovernmentForm.valid).toBeFalsy();
    component.localGovernmentForm.controls['name'].markAsDirty();
    component.onValueChanged();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.formErrors['name']).toEqual('Name is required. ');
    });
  }));

  it('should create a local government and redirect to the local government list', () => {
    params.next({});
    expect(component.localGovernmentForm.valid).toBeFalsy();
    component.localGovernmentForm.controls['name'].setValue('TEST');
    component.localGovernmentForm.controls['siren'].setValue('000000000');
    component.localGovernmentForm.controls['subscription_id'].setValue('1');
    expect(component.localGovernmentForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/local_governments']);

    // Now we can make sure the local government has been correctly affected
    expect(component.localGovernment.name).toBe('TEST');
    expect(component.localGovernment.siren).toBe('000000000');
  });

  it('should pre-render the form when updating a local government', () => {
    params.next({'id': 2});
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.ngOnInit();
      expect(component.localGovernment.name).toEqual('COMMUNE DE MONTPELLIER');
      expect(component.localGovernment.siren).toEqual('213401722');
    });
  });

  it('should edit an existing local government and redirect to the local government list', () => {
    params.next({'id': 2});
    component.localGovernmentForm.controls['siren'].setValue('000000000');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/local_governments']);
    expect(component.localGovernment.siren).toEqual('000000000');
    expect(component.localGovernment.name).toEqual('COMMUNE DE MONTPELLIER');
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
