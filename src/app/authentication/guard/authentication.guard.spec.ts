import {TestBed, async, inject} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Router, RouterStateSnapshot} from '@angular/router';

import {AuthenticationGuard} from './authentication.guard';
import {AuthenticationService} from '../shared/authentication.service';
import {AuthenticationServiceMock} from '../shared/authentication.service.mock';

describe('AuthenticationGuard', () => {
  let mockSnapshot: RouterStateSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationGuard,
        {provide: AuthenticationService, useClass: AuthenticationServiceMock},
      ],
      imports: [RouterTestingModule]
    });
    mockSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  });

  it('should ...', inject([AuthenticationGuard], (guard: AuthenticationGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should activate the page', inject([AuthenticationGuard, Router], (guard: AuthenticationGuard, router: Router) => {
    spyOn(router, 'navigate');

    expect(guard.canActivate(null, null)).toBeTruthy();
    expect(router.navigate).not.toHaveBeenCalled();
  }));

  it('should not activate the page and redirect the user',
    inject([AuthenticationGuard, Router, AuthenticationService],
      (guard: AuthenticationGuard, router: Router, mockAuth: AuthenticationService) => {
        mockSnapshot.url = '/dashboard';
        spyOn(router, 'navigate');

        spyOn(mockAuth, 'isAuthenticated').and.returnValue(false);

        expect(guard.canActivate(null, mockSnapshot)).toBeFalsy();
        expect(router.navigate).toHaveBeenCalled();
      }));
});
