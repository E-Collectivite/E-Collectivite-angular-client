import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';

import {AuthenticationService} from '../shared/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  redirectUrl: string;

  constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private router: Router) {
    // get redirect url from route parameters or default to /
    this.redirectUrl = this.route.snapshot.queryParams['redirectUrl'] || '/';
  }

  ngOnInit() {
    this.authenticationService.authenticate()
      .subscribe(result => {
          // login successful so redirect the user
          this.router.navigateByUrl(this.redirectUrl);
        },
        error => {
          location.href = environment.loginUrl + '?redirect_url=' + this.router.url;
        });
  }

}
