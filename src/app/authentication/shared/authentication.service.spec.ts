import {TestBed, inject} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {encodeTestToken} from 'angular2-jwt/angular2-jwt-test-helpers';

import {AuthenticationService} from './authentication.service';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationService,
      ],
      imports: [
        HttpClientTestingModule,
      ],
    });
  });

  it('should be created', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));

  describe('authenticate()', () => {

    it('should authenticate a user', inject([AuthenticationService, HttpTestingController],
      (authenticationService: AuthenticationService, httpMock: HttpTestingController) => {
        const expectedBody = {
          token: encodeTestToken({'sub': 2, 'name': 'mreyrolle', 'exp': 9999999999}),
          user: {
            id: 2,
            name: 'mreyrolle',
            mail: 'maxime.reyrolle@example.com',
            firstname: 'Maxime',
            lastname: 'REYROLLE',
            superadmin: false
          }
        };

        // An HTTP GET request is made in the authenticate() method, expect that the authenticate() method return true.
        authenticationService.authenticate().subscribe(data => {
          expect(data).toBeTruthy();
        });

        // The request is pending, and no response has been sent. Expect that the request happened.
        const req = httpMock.expectOne('https://cake.test.adullact.org/api/v1/users/token.json');

        // If no request with that URL was made, or if multiple requests match, expectOne() would throw.
        // However this test makes only one request to this URL, so it will match and return a mock request.
        // The mock request can be used to deliver a response or make assertions against the request.
        // In this case, the test asserts that the request is a GET.
        expect(req.request.method).toEqual('GET');

        // Fulfill the request by transmitting a response.
        req.flush(expectedBody);

        // Assert that there are no outstanding requests.
        httpMock.verify();
      }));

    it('should not authenticate a user', inject([AuthenticationService, HttpTestingController],
      (authenticationService: AuthenticationService, httpMock: HttpTestingController) => {
        const expectedBody = {};

        authenticationService.authenticate().subscribe(data => {
          expect(data).toBeFalsy();
        });

        const req = httpMock.expectOne('https://cake.test.adullact.org/api/v1/users/token.json');

        expect(req.request.method).toEqual('GET');
        req.flush(expectedBody);
        httpMock.verify();
      }));
  });

  describe('logout()', () => {
    it('should remove token from localStorage when logging out a user', inject([AuthenticationService],
      (authenticationService: AuthenticationService) => {
        spyOn(localStorage, 'removeItem').and.callThrough();
        authenticationService.logout();
        expect(localStorage.removeItem).toHaveBeenCalledWith('token');
        expect(localStorage.removeItem).toHaveBeenCalledWith('user');
      }));
  });

  describe('isAuthenticated()', () => {

    it('should return false when no token', inject([AuthenticationService],
      (authenticationService: AuthenticationService) => {
        expect(authenticationService.isAuthenticated()).toBeFalsy();
      }));

    it('should return false when token is expired', inject([AuthenticationService],
      (authenticationService: AuthenticationService) => {
        const expiredToken = encodeTestToken({'sub': 1, 'name': 'my_user', 'exp': 0});
        localStorage.setItem('token', expiredToken);
        expect(authenticationService.isAuthenticated()).toBeFalsy();
      }));

    it('should return true when token is not expired', inject([AuthenticationService],
      (authenticationService: AuthenticationService) => {
        const validToken = encodeTestToken({'sub': 1, 'name': 'my_user', 'exp': 9999999999});
        localStorage.setItem('token', validToken);
        expect(authenticationService.isAuthenticated()).toBeTruthy();
      }));
  });
});
