import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {tokenNotExpired} from 'angular2-jwt';

import {User} from '../../users/shared/user.model';

@Injectable()
export class AuthenticationService {
  private url = environment.apiUrl + '/users/token.json';

  constructor(private http: HttpClient) {
  }

  authenticate(): Observable<boolean> {
    return this.http.get<{ token: string, user: User }>(this.url, {withCredentials: true})
      .map(response => {
          // successful login if there is a jwt in the response
          const token = response.token;
          const user = response.user;
          if (token) {
            // store user and token in local storage to keep user logged in
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(user));
            return true;
          }
          return false;
        }
      );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  isAuthenticated(): boolean {
    return tokenNotExpired();
  }
}
