import {Observable} from 'rxjs/Observable';

export class AuthenticationServiceMock {

  authenticate(): Observable<boolean> {
    return Observable.of(true);
  }

  isAuthenticated(): boolean {
    return true;
  }
}

