import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UsersModule} from './users/users.module';
import {LocalGovernmentsModule} from './local-governments/local-governments.module';
import {ConnectorsModule} from './connectors/connectors.module';
import {SubscriptionsModule} from './subscriptions/subscriptions.module';
import {AuthenticationModule} from './authentication/authentication.module';
import {AuthenticationService} from './authentication/shared/authentication.service';
import {AuthenticationGuard} from './authentication/guard/authentication.guard';
import {DashboardModule} from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    UsersModule,
    LocalGovernmentsModule,
    ConnectorsModule,
    SubscriptionsModule,
    AuthenticationModule,
    DashboardModule
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
