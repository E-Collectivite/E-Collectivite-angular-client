import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {Http, BaseRequestOptions, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {DashboardService} from './dashboard.service';

describe('DashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DashboardService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([DashboardService], (service: DashboardService) => {
    expect(service).toBeTruthy();
  }));

  describe('getDataFromServices()', () => {

    it('should load data from services', inject([DashboardService, MockBackend],
      fakeAsync((dashboardService: DashboardService, mockBackend: MockBackend) => {
        const expectedBody = {
          'services': [
            {
              'name': 'Actes',
              'url': 'https://pastell.example.org',
              'data': [
                {
                  'label': '0 documents with TDT errors',
                  'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=erreur-verif-tdt'
                },
                {
                  'label': '1 pending document',
                  'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=modification'
                },
                {
                  'label': '0 documents with prefecture response',
                  'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=verif-reponse-tdt'
                }
              ]
            },
            {
              'name': 'Hélios',
              'url': 'https://pastell.example.org',
              'data': [
                {
                  'label': '1 document with TDT error',
                  'link': 'https://pastell.example.org/document/list.php?type=helios-generique&id_e=1&filtre=tdt-error'
                },
                {
                  'label': '0 documents to be transmitted to the TDT',
                  'link': 'https://pastell.example.org/document/list.php?type=helios-generique&id_e=1&filtre=recu-iparapheur'
                }
              ]
            }
          ]
        };
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/local_governments/3/services.json');
          const options = new ResponseOptions({status: 200, body: expectedBody});
          connection.mockRespond(new Response(options));
        });

        dashboardService.getDataFromServices(3).subscribe(data => {
          result = data;
        });

        tick();

        expect(result).toBe(expectedBody, 'should return the data from the services');
      })));
  });
});
