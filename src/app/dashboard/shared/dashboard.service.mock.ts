import {Observable} from 'rxjs/Observable';

export class DashboardServiceMock {

  static SERVICE_1 = {
    'name': 'Actes',
    'url': 'https://pastell.example.org',
    'data': [
      {
        'label': '0 documents with TDT errors',
        'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=erreur-verif-tdt'
      },
      {
        'label': '1 pending document',
        'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=modification'
      },
      {
        'label': '0 documents with prefecture response',
        'link': 'https://pastell.example.org/document/list.php?type=actes-generique&id_e=1&filtre=verif-reponse-tdt'
      }
    ]
  };
  static SERVICE_2 = {
    'name': 'Hélios',
    'url': 'https://pastell.example.org',
    'data': [
      {
        'label': '1 document with TDT error',
        'link': 'https://pastell.example.org/document/list.php?type=helios-generique&id_e=1&filtre=tdt-error'
      },
      {
        'label': '0 documents to be transmitted to the TDT',
        'link': 'https://pastell.example.org/document/list.php?type=helios-generique&id_e=1&filtre=recu-iparapheur'
      }
    ]
  };

  static services = [DashboardServiceMock.SERVICE_1, DashboardServiceMock.SERVICE_2];


  getDataFromServices(local_government_id) {
    return Observable.of({'services': DashboardServiceMock.services});
  }
}
