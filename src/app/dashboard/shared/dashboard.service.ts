import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {environment} from '../../../environments/environment';

@Injectable()
export class DashboardService {
  private url = environment.apiUrl + '/local_governments';

  constructor(private authHttp: AuthHttp) {
  }

  getDataFromServices(local_government_id) {
    return this.authHttp.get(this.url + '/' + local_government_id + '/services.json')
      .map(response => response.json());
  }
}
