import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {By} from '@angular/platform-browser';

import {DashboardComponent} from './dashboard.component';
import {DashboardService} from '../shared/dashboard.service';
import {DashboardServiceMock} from '../shared/dashboard.service.mock';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const mock = {
    params: Observable.of({local_government_id: 1})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent
      ],
      providers: [
        {provide: DashboardService, useClass: DashboardServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return 2 services', () => {
    fixture.detectChanges();
    const services = fixture.debugElement.queryAll(By.css('.services'));
    expect(services.length).toEqual(2);
  });

  it('should display labels of the first service data', () => {
    fixture.detectChanges();
    const services = fixture.debugElement.queryAll(By.css('.services'));
    const serviceTextContent = services[0].nativeElement.textContent;
    expect(serviceTextContent).toContain(DashboardServiceMock.SERVICE_1.data[0].label);
    expect(serviceTextContent).toContain(DashboardServiceMock.SERVICE_1.data[1].label);
    expect(serviceTextContent).toContain(DashboardServiceMock.SERVICE_1.data[2].label);
  });
});
