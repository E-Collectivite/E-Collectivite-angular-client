import {Component, OnInit} from '@angular/core';

import {DashboardService} from '../shared/dashboard.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  servicesData;

  constructor(private dashboardService: DashboardService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (!params['local_government_id']) {
        return;
      }
      this.dashboardService.getDataFromServices(params['local_government_id'])
        .subscribe(servicesData => {
          this.servicesData = servicesData.services;
        });
    });
  }

}
