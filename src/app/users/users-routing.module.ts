import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {UserListComponent} from './user-list/user-list.component';
import {UserComponent} from './user/user.component';
import {UserFormComponent} from './user-form/user-form.component';
import {AuthenticationGuard} from '../authentication/guard/authentication.guard';

const routes: Routes = [
  {
    path: 'users',
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: UserListComponent
      },
      {
        path: 'add',
        component: UserFormComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            component: UserComponent
          },
          {
            path: 'edit',
            component: UserFormComponent
          },
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
