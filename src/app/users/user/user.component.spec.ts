import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserComponent} from './user.component';
import {RouterTestingModule} from '@angular/router/testing';
import {UserService} from '../shared/user.service';
import {UserServiceMock} from '../shared/user.service.mock';
import {By} from '@angular/platform-browser';
import {ActivatedRoute, Params} from '@angular/router';
import {Subject} from 'rxjs/Subject';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;
  let params: Subject<Params>;
  beforeEach(async(() => {
    params = new Subject<Params>();
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [UserComponent],
      providers: [
        {provide: UserService, useClass: UserServiceMock},
        { provide: ActivatedRoute, useValue: { params: params } }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should return only one user', () => {
    params.next({ 'id': 1});
    fixture.detectChanges();
    const user = fixture.debugElement.queryAll(By.css('ul'));
    expect(user.length).toEqual(1);
  });

  it('first user should match first test user', () => {
    params.next({ 'id': 1});
    fixture.detectChanges();
    const user = fixture.debugElement.queryAll(By.css('ul'));
    const userTextContent = user[0].nativeElement.textContent;
    expect(userTextContent).toContain(UserServiceMock.USER_1.name);
    expect(userTextContent).toContain(UserServiceMock.USER_1.mail);
    expect(userTextContent).toContain(UserServiceMock.USER_1.firstname);
    expect(userTextContent).toContain(UserServiceMock.USER_1.lastname);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
