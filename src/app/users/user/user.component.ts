import {Component, OnInit} from '@angular/core';
import {UserService} from '../shared/user.service';
import {ActivatedRoute} from '@angular/router';
import {User} from '../shared/user.model';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.userService.getUser(params['id'])
    ).subscribe(user => this.user = user);
  }

}
