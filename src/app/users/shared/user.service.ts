import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {environment} from '../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {User} from './user.model';

@Injectable()
export class UserService {
  private url = environment.apiUrl + '/users';

  constructor(private authHttp: AuthHttp) {
  }

  // TODO: expect pagination
  getUsers(): Observable<{ users: User[] }> {
    return this.authHttp.get(this.url + '.json')
      .map(response => response.json());
  }

  getUser(id): Observable<User> {
    return this.authHttp.get(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  delete(id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  addUser(user: User): Observable<User> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(user);

    return this.authHttp.post(this.url + '.json', body, options)
      .map(response => response.json());
  }

  editUser(user: User): Observable<User> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(user);

    return this.authHttp.put(this.url + '/' + user.id + '.json', body, options)
      .map(response => response.json());
  }

}
