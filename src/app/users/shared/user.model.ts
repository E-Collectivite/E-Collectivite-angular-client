export class User {
  id?: number;
  name: string;
  mail: string;
  firstname: string;
  lastname: string;
  superadmin?: boolean;
  password?: string;
}
