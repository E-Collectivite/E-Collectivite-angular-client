import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {UserService} from './user.service';

describe('UserService', () => {

  const USER_1 = {
    id: 1,
    name: 'admin',
    mail: 'admin@example.com',
    firstname: 'admin',
    lastname: 'admin',
    superadmin: true
  };
  const USER_2 = {
    id: 2,
    name: 'mreyrolle',
    mail: 'maxime.reyrolle@example.com',
    firstname: 'Maxime',
    lastname: 'REYROLLE',
    superadmin: false
  };
  const USERS = [USER_1, USER_2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  describe('getUsers()', () => {

    it('should load all users', inject([UserService, MockBackend], fakeAsync((userService: UserService, mockBackend: MockBackend) => {
      let result;

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Get);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users.json');
        const options = new ResponseOptions({status: 200, body: USERS});
        connection.mockRespond(new Response(options));
      });

      userService.getUsers().subscribe(users => {
        result = users;
      });

      tick();

      expect(result.length).toBe(USERS.length, 'should have expected number of users');
      expect(result).toBe(USERS, 'should return the user list');
    })));

    it('should be OK returning no users', inject([UserService, MockBackend],
      fakeAsync((userService: UserService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        userService.getUsers().subscribe(users => {
          result = users;
        });

        tick();
        expect(result.length).toBe(0, 'should have no users');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('getUser(id)', () => {
    it('should load a user', inject([UserService, MockBackend], fakeAsync((userService: UserService, mockBackend: MockBackend) => {
      let result;

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Get);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users/1.json');
        const options = new ResponseOptions({status: 200, body: USER_1});
        connection.mockRespond(new Response(options));
      });

      userService.getUser(1).subscribe(user => {
        result = user;
      });

      tick();

      expect(result).toBe(USER_1, 'should return only one user');
    })));
  });

  describe('delete()', () => {
    it('should delete a user', inject([UserService, MockBackend], fakeAsync((userService: UserService, mockBackend: MockBackend) => {
      let result;
      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Delete);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users/1.json');
        const options = new ResponseOptions({status: 204});
        connection.mockRespond(new Response(options));
      });

      userService.delete(USER_1.id).subscribe(user => {
        result = user;
      });
      tick();

      expect(result).toBe(null, 'should return nothing');
    })));
  });

  describe('addUser()', () => {

    it('should create a user', inject([UserService, MockBackend], fakeAsync((userService: UserService, mockBackend: MockBackend) => {
      let result;
      const newUser = {
        name: 'admin',
        mail: 'admin@example.com',
        firstname: 'admin',
        lastname: 'admin'
      };

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Post);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users.json');
        const options = new ResponseOptions({status: 201, body: USER_1});
        connection.mockRespond(new Response(options));
      });

      userService.addUser(newUser).subscribe(user => {
        result = user;
      });
      tick();

      expect(result).toBe(USER_1);
    })));

  });

  describe('editUser()', () => {

    it('should edit a user', inject([UserService, MockBackend], fakeAsync((userService: UserService, mockBackend: MockBackend) => {
      let result;
      const updatedUser = USER_2;
      updatedUser.firstname = 'test firstname';

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Put);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/users/2.json');
        const options = new ResponseOptions({status: 201, body: updatedUser});
        connection.mockRespond(new Response(options));
      });

      userService.editUser(updatedUser).subscribe(user => {
        result = user;
      });
      tick();

      expect(result).toBe(updatedUser);
    })));

  });
});
