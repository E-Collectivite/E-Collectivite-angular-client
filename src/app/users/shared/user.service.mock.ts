import {User} from './user.model';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Response, ResponseOptions} from '@angular/http';

export class UserServiceMock {
  static USER_1 = {
    id: 1,
    name: 'admin',
    mail: 'admin@example.com',
    firstname: 'admin',
    lastname: 'admin',
    superadmin: true
  };
  static USER_2 = {
    id: 2,
    name: 'mreyrolle',
    mail: 'maxime.reyrolle@example.com',
    firstname: 'Maxime',
    lastname: 'REYROLLE',
    superadmin: false
  };

  static users = [UserServiceMock.USER_1, UserServiceMock.USER_2];

  getUsers(): Observable<{ users: User[] }> {
    return Observable.of({users: UserServiceMock.users});
  }

  getUser(id): Observable<User> {
    const user = UserServiceMock.users.find(currentUser => currentUser.id === id);
    return Observable.of(user);
  }

  delete(id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

  addUser(user: User): Observable<User> {
    return Observable.of(user);
  }

  editUser(user: User): Observable<User> {
    return Observable.of(user);
  }

}
