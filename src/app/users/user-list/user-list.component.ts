import {Component, OnInit} from '@angular/core';
import {UserService} from '../shared/user.service';
import {User} from '../shared/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  errorMessage: string;
  users: any;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    // TODO: get pagination
    this.userService.getUsers()
      .subscribe(users => this.users = users.users);
  }

  delete(user: User) {
    // @see angular2-modal or angular-confirmation-popover for better integration with angular
    if (confirm('Are you sure to delete ' + user.name + ' ?')) {
      this.userService.delete(user.id)
        .subscribe(response => {
          // Update the current list of users without refreshing the page (save 1 request)
          this.users = this.users
            .filter(oldUsers => oldUsers.id !== user.id);
          return this;
        });
    }
  }

}
