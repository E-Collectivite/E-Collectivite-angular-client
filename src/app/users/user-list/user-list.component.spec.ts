import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserListComponent} from './user-list.component';
import {UserService} from '../shared/user.service';
import {UserServiceMock} from '../shared/user.service.mock';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [UserListComponent],
      providers: [
        {provide: UserService, useClass: UserServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display users', () => {
    const users = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(users.length).toEqual(2);
  });

  it('first user should match first test user', () => {
    const users = fixture.debugElement.queryAll(By.css('tbody tr'));
    const user = users[0].nativeElement.textContent;
    expect(user).toContain(UserServiceMock.users[0].name);
    expect(user).toContain(UserServiceMock.users[0].mail);
    expect(user).toContain(UserServiceMock.users[0].firstname);
    expect(user).toContain(UserServiceMock.users[0].lastname);
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'delete').and.callThrough();

    const mockService = fixture.debugElement.injector.get(UserService);
    spyOn(mockService, 'delete').and.callThrough();

    const users = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = users[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.delete).toHaveBeenCalled();

    // const deleteButton = users[0].nativeElement.querySelector('button');
    // deleteButton.click();
    // fixture.whenStable().then(() => {
    //   expect(component.delete).toHaveBeenCalled();
    // });
    expect(mockService.delete).toHaveBeenCalled();
  });

  it('should not call the user service delete method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'delete').and.callThrough();
    const mockService = fixture.debugElement.injector.get(UserService);
    spyOn(mockService, 'delete').and.callThrough();

    const users = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = users[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.delete).toHaveBeenCalled();
    expect(mockService.delete).not.toHaveBeenCalled();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
