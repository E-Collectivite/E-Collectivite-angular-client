import {Component, OnInit} from '@angular/core';
import {User} from '../shared/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../shared/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  id = null;
  user: User = new User();
  userForm: FormGroup;
  formErrors = {
    'name': '',
    'password': '',
    'mail': '',
    'firstname': '',
    'lastname': '',
  };

  validationMessages = {
    'name': {
      'required': 'Name is required.',
    },
    'password': {
      'required': 'Password is required.',
    },
    'mail': {
      'required': 'E-mail is required.',
      'email': 'Invalid email',
    },
    'firstname': {
      'required': 'Firstname is required.',
    },
    'lastname': {
      'required': 'Lastname is required.',
    }
  };

  constructor(private userService: UserService, private fb: FormBuilder, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (!params['id']) {
        return;
      }
      this.id = params['id'];
      this.userService.getUser(this.id)
        .subscribe(user => this.user = user);
    });

    this.buildForm();
  }

  private buildForm() {
    this.userForm = this.fb.group({
      'name': [this.user.name, [Validators.required]],
      'mail': [this.user.mail, [Validators.required, Validators.email]],
      'password': [this.user.password, [Validators.required]],
      'firstname': [this.user.firstname, [Validators.required]],
      'lastname': [this.user.lastname, [Validators.required]],
      'superadmin': [this.user.superadmin]
    });

    if (this.id) {
      this.userForm.controls['password'].clearValidators();
      this.userForm.controls['name'].disable();
    }

    this.userForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.userForm) {
      return;
    }
    const form = this.userForm;

    for (const field of Object.keys(this.formErrors)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  submit() {
    this.user = Object.assign(this.user, this.userForm.value);
    if (this.user.id) {
      this.userService.editUser(this.user).subscribe(
        result => this.router.navigate(['/users']));
    } else {
      this.userService.addUser(this.user).subscribe(
        result => this.router.navigate(['/users']));
    }
  }

}
