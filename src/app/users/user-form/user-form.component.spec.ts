import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserFormComponent} from './user-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UserService} from '../shared/user.service';
import {UserServiceMock} from '../shared/user.service.mock';
import {Subject} from 'rxjs/Subject';
import {Params, Router, ActivatedRoute} from '@angular/router';

describe('UserFormComponent', () => {
  let component: UserFormComponent;
  let fixture: ComponentFixture<UserFormComponent>;
  let params: Subject<Params>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    params = new Subject<Params>();
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [UserFormComponent],
      providers: [
        {provide: UserService, useClass: UserServiceMock},
        {provide: ActivatedRoute, useValue: {params: params}},
        {provide: Router, useValue: router}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be invalid when form is empty', () => {
    params.next({});
    expect(component.userForm.valid).toBeFalsy();
  });

  it('check the email field', () => {
    params.next({});
    const email = component.userForm.controls['mail'];
    expect(email.valid).toBeFalsy();

    // Email field is required
    expect(email.errors['required']).toBeTruthy();
    // Email field should be valid
    expect(email.errors['email']).toBeTruthy();

    // Set email to random string
    email.setValue('test');
    expect(email.errors['required']).toBeFalsy();
    expect(email.errors['email']).toBeTruthy();

    // Set email to something correct
    email.setValue('test@example.com');
    expect(email.valid).toBeTruthy();
  });

  it('check that the validation errors are displayed to the user', async(() => {
    params.next({});
    expect(component.userForm.valid).toBeFalsy();
    component.userForm.controls['password'].markAsDirty();
    component.onValueChanged();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.formErrors['password']).toEqual('Password is required. ');
    });
  }));

  it('should create a user and redirect to the user list', () => {
    params.next({});
    expect(component.userForm.valid).toBeFalsy();
    component.userForm.controls['name'].setValue('admin');
    component.userForm.controls['mail'].setValue('admin@example.com');
    component.userForm.controls['password'].setValue('admin');
    component.userForm.controls['firstname'].setValue('admin');
    component.userForm.controls['lastname'].setValue('admin');
    expect(component.userForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/users']);

    // Now we can make sure the user has been correctly affected
    expect(component.user.name).toBe('admin');
    expect(component.user.mail).toBe('admin@example.com');
  });

  it('should pre-render the form when updating a user', () => {
    params.next({'id': 2});
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.ngOnInit();
      expect(component.userForm.controls['name'].disabled).toBeTruthy();
      expect(component.user.name).toEqual('mreyrolle');
      expect(component.user.mail).toEqual('maxime.reyrolle@example.com');
    });
  });

  it('should edit an existing user and redirect to the user list', () => {
    params.next({'id': 2});
    component.userForm.controls['firstname'].setValue('TEST username');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/users']);
    expect(component.user.firstname).toEqual('TEST username');
    expect(component.user.name).toEqual('mreyrolle');
    expect(component.user.mail).toEqual('maxime.reyrolle@example.com');
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
