import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

import {ConnectorListComponent} from './connector-list.component';
import {ConnectorService} from '../shared/connector.service';
import {ConnectorServiceMock} from '../shared/connector.service.mock';

describe('ConnectorListComponent', () => {
  let component: ConnectorListComponent;
  let fixture: ComponentFixture<ConnectorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ConnectorListComponent],
      providers: [
        {provide: ConnectorService, useClass: ConnectorServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display connectors', () => {
    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(connectors.length).toEqual(2);
  });

  it('should match first test connector', () => {
    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));
    const connector = connectors[0].nativeElement.textContent;
    expect(connector).toContain(ConnectorServiceMock.CONNECTOR_1.name);
    expect(connector).toContain(ConnectorServiceMock.CONNECTOR_1.label);
    expect(connector).toContain(ConnectorServiceMock.CONNECTOR_1.url);
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'deleteConnector').and.callThrough();

    const mockService = fixture.debugElement.injector.get(ConnectorService);
    spyOn(mockService, 'deleteConnector').and.callThrough();

    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = connectors[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteConnector).toHaveBeenCalled();
    expect(mockService.deleteConnector).toHaveBeenCalled();
  });

  it('should not call the connector service remove method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'deleteConnector').and.callThrough();
    const mockService = fixture.debugElement.injector.get(ConnectorService);
    spyOn(mockService, 'deleteConnector').and.callThrough();

    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = connectors[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteConnector).toHaveBeenCalled();
    expect(mockService.deleteConnector).not.toHaveBeenCalled();
  });
});
