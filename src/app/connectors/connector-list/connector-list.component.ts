import {Component, OnInit} from '@angular/core';

import {Connector} from '../shared/connector.model';
import {ConnectorService} from '../shared/connector.service';

@Component({
  selector: 'app-connector-list',
  templateUrl: './connector-list.component.html',
  styleUrls: ['./connector-list.component.css']
})
export class ConnectorListComponent implements OnInit {
  connectors: Connector[];

  constructor(private connectorService: ConnectorService) {
  }

  ngOnInit() {
    // TODO: get pagination
    this.connectorService.getConnectors()
      .subscribe(connectors => this.connectors = connectors.connectors);
  }

  deleteConnector(connector: Connector) {
    if (confirm('Are you sure to remove ' + connector.name + ' ?')) {
      this.connectorService.deleteConnector(connector.id)
        .subscribe(response => {
          // Update the current list of connectors
          this.connectors = this.connectors
            .filter(oldSubscription => oldSubscription.id !== connector.id);
          return this;
        });
    }
  }
}
