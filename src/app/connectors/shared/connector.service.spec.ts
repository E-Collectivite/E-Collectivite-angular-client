import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {ConnectorService} from './connector.service';

describe('ConnectorService', () => {

  const CONNECTOR_1 = {
    id: 1,
    name: 'pastell',
    label: 'Pastell',
    url: 'https://pastell.example.org',
  };
  const CONNECTOR_2 = {
    id: 2,
    name: 'parapheur',
    label: 'i-Parapheur',
    url: 'https://iparapheur.example.org',
  };
  const CONNECTORS = [CONNECTOR_1, CONNECTOR_2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConnectorService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([ConnectorService], (service: ConnectorService) => {
    expect(service).toBeTruthy();
  }));

  describe('getConnectors()', () => {

    it('should load all connectors', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors.json');
          const options = new ResponseOptions({status: 200, body: CONNECTORS});
          connection.mockRespond(new Response(options));
        });

        connectorService.getConnectors().subscribe(connectors => {
          result = connectors;
        });

        tick();

        expect(result.length).toBe(CONNECTORS.length, 'should have expected number of connectors');
        expect(result).toBe(CONNECTORS, 'should return the connector list');
      })));


    it('should be OK returning no connectors', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        connectorService.getConnectors().subscribe(connectors => {
          result = connectors;
        });

        tick();
        expect(result.length).toBe(0, 'should have no connectors');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('addConnector()', () => {

    it('should create a connector', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;
        const newConnector = {
          name: 'pastell',
          label: 'Pastell',
          url: 'https://pastell.example.org'
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors.json');
          const options = new ResponseOptions({status: 201, body: CONNECTOR_1});
          connection.mockRespond(new Response(options));
        });

        connectorService.addConnector(newConnector).subscribe(connector => {
          result = connector;
        });
        tick();

        expect(result).toBe(CONNECTOR_1);
      })));
  });

  describe('editConnector()', () => {

    it('should edit a connector', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;
        const updatedConnector = CONNECTOR_1;
        updatedConnector.label = 'Pastell - test';

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Put);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1.json');
          const options = new ResponseOptions({status: 201, body: updatedConnector});
          connection.mockRespond(new Response(options));
        });

        connectorService.editConnector(updatedConnector).subscribe(connector => {
          result = connector;
        });
        tick();

        expect(result).toBe(updatedConnector);
      })));
  });

  describe('getConnector(id)', () => {
    it('should load a connector', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/2.json');
          const options = new ResponseOptions({status: 200, body: CONNECTOR_2});
          connection.mockRespond(new Response(options));
        });

        connectorService.getConnector(2).subscribe(connector => {
          result = connector;
        });

        tick();

        expect(result).toBe(CONNECTOR_2, 'should return only one connector');
      })));
  });

  describe('deleteConnector()', () => {
    it('should delete a connector', inject([ConnectorService, MockBackend],
      fakeAsync((connectorService: ConnectorService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/2.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        connectorService.deleteConnector(CONNECTOR_2.id).subscribe(connector => {
          result = connector;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });
});
