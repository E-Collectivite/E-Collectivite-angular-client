import {Observable} from 'rxjs/Observable';
import {ResponseOptions, Response} from '@angular/http';

import {Connector} from './connector.model';

export class ConnectorServiceMock {
  static CONNECTOR_1 = {
    id: 1,
    name: 'pastell',
    label: 'Pastell',
    url: 'https://pastell.example.org',
  };
  static CONNECTOR_2 = {
    id: 2,
    name: 'parapheur',
    label: 'i-Parapheur',
    url: 'https://iparapheur.example.org',
  };
  static connectors = [ConnectorServiceMock.CONNECTOR_1, ConnectorServiceMock.CONNECTOR_2];

  getConnectors(): Observable<{ connectors: Connector[] }> {
    return Observable.of({connectors: ConnectorServiceMock.connectors});
  }

  addConnector(connector: Connector): Observable<Connector> {
    return Observable.of(connector);
  }

  editConnector(connector: Connector): Observable<Connector> {
    return Observable.of(connector);
  }

  getConnector(id): Observable<Connector> {
    const connector = ConnectorServiceMock.connectors.find(currentConnector => currentConnector.id === id);
    return Observable.of(connector);
  }

  deleteConnector(id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }
}
