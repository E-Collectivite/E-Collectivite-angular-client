import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {Connector} from './connector.model';

@Injectable()
export class ConnectorService {
  private url = environment.apiUrl + '/connectors';

  constructor(private authHttp: AuthHttp) {
  }

  getConnectors(): Observable<{ connectors: Connector[] }> {
    return this.authHttp.get(this.url + '.json')
      .map(response => response.json());
  }

  addConnector(connector: Connector): Observable<Connector> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(connector);

    return this.authHttp.post(this.url + '.json', body, options)
      .map(response => response.json());
  }

  editConnector(connector: Connector): Observable<Connector> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(connector);

    return this.authHttp.put(this.url + '/' + connector.id + '.json', body, options)
      .map(response => response.json());
  }

  getConnector(id): Observable<Connector> {
    return this.authHttp.get(this.url + '/' + id + '.json')
      .map(response => response.json());
  }

  deleteConnector(id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + id + '.json')
      .map(response => response.json());
  }
}
