export class Connector {
  id?: number;
  name: string;
  url: string;
  label: string;
}
