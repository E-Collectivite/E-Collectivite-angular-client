import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ConnectorListComponent} from './connector-list/connector-list.component';
import {ConnectorFormComponent} from './connector-form/connector-form.component';
import {ConnectorComponent} from './connector/connector.component';
import {AuthenticationGuard} from '../authentication/guard/authentication.guard';

const routes: Routes = [
  {
    path: 'connectors',
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: ConnectorListComponent,
      },
      {
        path: 'add',
        component: ConnectorFormComponent
      },
      {
        path: ':connector_id',
        children: [
          {
            path: '',
            component: ConnectorComponent,
          },
          {
            path: 'edit',
            component: ConnectorFormComponent
          },
          {
            path: 'services',
            loadChildren: 'app/connectors/services/services.module#ServicesModule',
          }
        ],
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnectorsRoutingModule {
}
