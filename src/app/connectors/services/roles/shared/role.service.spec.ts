import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {RoleService} from './role.service';

describe('RoleService', () => {

  const ROLE_1 = {
    id: 1,
    name: 'Agent actes',
    service_id: 1
  };
  const ROLE_2 = {
    id: 2,
    name: 'Agent helios',
    service_id: 2
  };
  const ROLES = [ROLE_1, ROLE_2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoleService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([RoleService], (service: RoleService) => {
    expect(service).toBeTruthy();
  }));

  describe('getRoles()', () => {

    it('should load all roles', inject([RoleService, MockBackend], fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
      let result;

      mockBackend.connections.subscribe(connection => {
        expect(connection.request.method).toBe(RequestMethod.Get);
        expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1/roles.json');
        const options = new ResponseOptions({status: 200, body: [ROLE_1]});
        connection.mockRespond(new Response(options));
      });

      roleService.getRoles(1, 1).subscribe(roles => {
        result = roles;
      });

      tick();

      expect(result.length).toBe(1, 'should have expected number of roles');
      expect(result).toEqual([ROLE_1], 'should return the role list');
    })));

    it('should be OK returning no roles', inject([RoleService, MockBackend],
      fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/15/roles.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        roleService.getRoles(1, 15).subscribe(roles => {
          result = roles;
        });

        tick();
        expect(result.length).toBe(0, 'should have no roles');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('getRole()', () => {
    it('should load a role', inject([RoleService, MockBackend],
      fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1/roles/1.json');
          const options = new ResponseOptions({status: 200, body: ROLE_1});
          connection.mockRespond(new Response(options));
        });

        roleService.getRole(1, 1, 1).subscribe(role => {
          result = role;
        });

        tick();

        expect(result).toBe(ROLE_1, 'should return only one role');
      })));
  });

  describe('deleteRole()', () => {
    it('should delete a role', inject([RoleService, MockBackend],
      fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1/roles/1.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        roleService.deleteRole(1, ROLE_1.service_id, ROLE_1.id).subscribe(role => {
          result = role;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });

  describe('addRole()', () => {

    it('should create a role', inject([RoleService, MockBackend],
      fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
        let result;
        const newRole = {
          name: 'Agent actes',
          service_id: 1
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1/roles.json');
          const options = new ResponseOptions({status: 201, body: ROLE_1});
          connection.mockRespond(new Response(options));
        });

        roleService.addRole(1, 1, newRole).subscribe(role => {
          result = role;
        });
        tick();

        expect(result).toBe(ROLE_1);
      })));
  });

  describe('editRole()', () => {

    it('should edit a role', inject([RoleService, MockBackend],
      fakeAsync((roleService: RoleService, mockBackend: MockBackend) => {
        let result;
        const updatedRole = ROLE_1;
        updatedRole.name = 'Agent test';

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Put);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1/roles/1.json');
          const options = new ResponseOptions({status: 200, body: updatedRole});
          connection.mockRespond(new Response(options));
        });

        roleService.editRole(1, updatedRole).subscribe(role => {
          result = role;
        });
        tick();

        expect(result).toBe(updatedRole);
      })));
  });

});
