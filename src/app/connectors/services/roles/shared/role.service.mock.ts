import {Observable} from 'rxjs/Observable';
import {ResponseOptions, Response} from '@angular/http';

import {Role} from './role.model';

export class RoleServiceMock {
  static ROLE_1 = {
    id: 1,
    name: 'Agent actes',
    service_id: 1
  };
  static ROLE_2 = {
    id: 2,
    name: 'Agent hélios',
    service_id: 2
  };

  static roles = [RoleServiceMock.ROLE_1, RoleServiceMock.ROLE_2];

  getRoles(connector_id, service_id): Observable<Role[]> {
    return Observable.of([RoleServiceMock.ROLE_1]);
  }

  getRole(connector_id, service_id, role_id): Observable<Role> {
    return Observable.of(RoleServiceMock.ROLE_1);
  }

  deleteRole(connector_id, service_id, role_id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }

  addRole(connector_id, service_id, role: Role): Observable<Role> {
    return Observable.of(role);
  }

  editRole(connector_id, role: Role): Observable<Role> {
    return Observable.of(role);
  }
}
