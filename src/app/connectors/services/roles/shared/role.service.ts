import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {environment} from '../../../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {Role} from './role.model';

@Injectable()
export class RoleService {
  private url = environment.apiUrl + '/connectors';

  constructor(private authHttp: AuthHttp) {
  }

  getRoles(connector_id, service_id): Observable<Role[]> {
    return this.authHttp.get(this.url + '/' + connector_id + '/services/' + service_id + '/roles.json')
      .map(response => response.json());
  }

  getRole(connector_id, service_id, role_id): Observable<Role> {
    return this.authHttp.get(this.url + '/' + connector_id + '/services/' + service_id + '/roles/' + role_id + '.json')
      .map(response => response.json());
  }

  deleteRole(connector_id, service_id, role_id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + connector_id + '/services/' + service_id + '/roles/' + role_id + '.json')
      .map(response => response.json());
  }

  addRole(connector_id, service_id, role: Role): Observable<Role> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(role);
    return this.authHttp.post(this.url + '/' + connector_id + '/services/' + service_id + '/roles.json', body, options)
      .map(response => response.json());
  }

  editRole(connector_id, role: Role): Observable<Role> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(role);

    return this.authHttp.put(this.url + '/' + connector_id + '/services/' + role.service_id + '/roles/' + role.id + '.json', body, options)
      .map(response => response.json());
  }
}
