import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {RoleService} from '../shared/role.service';
import {Role} from '../shared/role.model';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css']
})
export class RoleFormComponent implements OnInit {
  connector_id: number;
  service_id: number;
  role_id: number;
  role: Role = new Role();
  roleForm: FormGroup;

  constructor(private roleService: RoleService, private fb: FormBuilder, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.buildForm();

    this.route.params.subscribe(params => {
      this.connector_id = params['connector_id'];
      this.service_id = params['service_id'];
      if (!params['role_id']) {
        return;
      }
      this.role_id = params['role_id'];
      this.roleService.getRole(this.connector_id, this.service_id, this.role_id)
        .subscribe(role => {
          this.role = role;
          this.roleForm.patchValue({
            'name': role.name
          });
        });
    });
  }

  private buildForm() {
    this.roleForm = this.fb.group({
      'name': [],
    });
  }

  submit() {
    this.role = Object.assign(this.role, this.roleForm.value);
    if (this.role.id) {
      this.roleService.editRole(this.connector_id, this.role).subscribe(
        result => this.router.navigate(['/connectors', this.connector_id, 'services', this.service_id, 'roles']));
    } else {
      this.roleService.addRole(this.connector_id, this.service_id, this.role).subscribe(
        result => this.router.navigate(['/connectors', this.connector_id, 'services', this.service_id, 'roles']));
    }
  }
}
