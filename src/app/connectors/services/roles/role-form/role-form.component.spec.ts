import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs/Subject';

import {RoleFormComponent} from './role-form.component';
import {RoleService} from '../shared/role.service';
import {RoleServiceMock} from '../shared/role.service.mock';

describe('RoleFormComponent', () => {
  let component: RoleFormComponent;
  let fixture: ComponentFixture<RoleFormComponent>;
  const params = new Subject<Params>();


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [RoleFormComponent],
      providers: [
        {provide: RoleService, useClass: RoleServiceMock},
        {provide: ActivatedRoute, useValue: {params}},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    params.next({
      'connector_id': 1,
      'service_id': 1
    });
    fixture = TestBed.createComponent(RoleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.roleForm.valid).toBeFalsy();
  });

  it('should be valid when name is set', () => {
    const name = component.roleForm.controls['name'];
    expect(name.valid).toBeFalsy();

    // name is required
    expect(name.errors['required']).toBeTruthy();

    name.setValue('TEST');
    expect(name.valid).toBeTruthy();

    expect(component.roleForm.valid).toBeTruthy();
  });

  it('should create the role', async(inject([Router], (router) => {
    spyOn(router, 'navigate');
    params.next({
      'connector_id': 1,
      'service_id': 1
    });
    expect(component.roleForm.valid).toBeFalsy();
    component.roleForm.controls['name'].setValue('TEST');
    expect(component.roleForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/connectors', 1, 'services', 1, 'roles']);
  })));

  it('should pre-render the form when updating a role', () => {
    params.next({
      'connector_id': 1,
      'service_id': 1,
      'role_id': 1
    });
    fixture.whenStable().then(() => {
      expect(component.roleForm.controls['name'].value).toEqual('Agent actes');
    });
  });

  it('should edit an existing role and redirect to the role list', async(inject([Router], (router) => {
    spyOn(router, 'navigate');
    params.next({
      'connector_id': 1,
      'service_id': 1,
      'role_id': 1
    });
    component.roleForm.controls['name'].setValue('Agent test');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/connectors', 1, 'services', 1, 'roles']);
    expect(component.role.name).toEqual('Agent test');
    expect(component.role.service_id).toEqual(1);
  })));
});
