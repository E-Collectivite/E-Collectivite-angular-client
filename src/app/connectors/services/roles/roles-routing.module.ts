import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {RoleListComponent} from './role-list/role-list.component';
import {RoleComponent} from './role/role.component';
import {RoleFormComponent} from './role-form/role-form.component';

const routes: Routes = [
  {
    path: '',
    component: RoleListComponent,
  },
  {
    path: 'add',
    component: RoleFormComponent,
  },
  {
    path: ':role_id',
    children: [
      {
        path: '',
        component: RoleComponent
      },
      {
        path: 'edit',
        component: RoleFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule {
}
