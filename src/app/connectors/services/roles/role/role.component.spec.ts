import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute} from '@angular/router';
import {By} from '@angular/platform-browser';

import {RoleComponent} from './role.component';
import {RoleService} from '../shared/role.service';
import {RoleServiceMock} from 'app/connectors/services/roles/shared/role.service.mock';

describe('RoleComponent', () => {
  let component: RoleComponent;
  let fixture: ComponentFixture<RoleComponent>;
  const mock = {
    params: Observable.of({
      connector_id: 1,
      service_id: 1,
      role_id: 1
    })
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoleComponent],
      providers: [
        {provide: RoleService, useClass: RoleServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should return a role', () => {
    const role = fixture.debugElement.queryAll(By.css('ul'));
    expect(role.length).toEqual(1);
  });

  it('should match the first test role', () => {
    const role = fixture.debugElement.queryAll(By.css('ul'));
    const roleTextContent = role[0].nativeElement.textContent;
    expect(roleTextContent).toContain(RoleServiceMock.ROLE_1.name);
  });
});
