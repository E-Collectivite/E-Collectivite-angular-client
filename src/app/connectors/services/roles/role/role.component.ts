import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Role} from '../shared/role.model';
import {RoleService} from '../shared/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  role: Role;

  constructor(private roleService: RoleService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.roleService.getRole(params['connector_id'], params['service_id'], params['role_id'])
    ).subscribe(role => this.role = role);
  }

}
