import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {RoleService} from '../shared/role.service';
import {Role} from '../shared/role.model';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
  roles: Role[];
  connectorId: number;
  serviceId: number;

  constructor(private roleService: RoleService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.connectorId = this.route.snapshot.parent.params['connector_id'];
    this.serviceId = this.route.snapshot.parent.params['service_id'];

    this.roleService.getRoles(this.connectorId, this.serviceId)
      .subscribe(roles => this.roles = roles);
  }

  deleteRole(role: Role) {
    if (confirm('Are you sure to remove ' + role.name + ' ?')) {
      this.roleService.deleteRole(this.connectorId, role.service_id, role.id)
        .subscribe(response => {
          // Update the current list of roles
          this.roles = this.roles
            .filter(oldRoles => oldRoles.id !== role.id);
          return this;
        });
    }
  }

}
