import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';

import {RoleListComponent} from './role-list.component';
import {RoleService} from '../shared/role.service';
import {RoleServiceMock} from '../shared/role.service.mock';

describe('RoleListComponent', () => {
  let component: RoleListComponent;
  let fixture: ComponentFixture<RoleListComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          connector_id: 1,
          service_id: 2
        }
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [RoleListComponent],
      providers: [
        {provide: RoleService, useClass: RoleServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display roles', () => {
    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(roles.length).toEqual(1);
  });

  it('first role should match first test role', () => {
    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));
    const role = roles[0].nativeElement.textContent;
    expect(role).toContain(RoleServiceMock.ROLE_1.name);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'deleteRole').and.callThrough();

    const mockService = fixture.debugElement.injector.get(RoleService);
    spyOn(mockService, 'deleteRole').and.callThrough();

    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = roles[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteRole).toHaveBeenCalled();
    expect(mockService.deleteRole).toHaveBeenCalled();
  });

  it('should not call the role service delete method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'deleteRole').and.callThrough();

    const mockService = fixture.debugElement.injector.get(RoleService);
    spyOn(mockService, 'deleteRole').and.callThrough();

    const roles = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = roles[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteRole).toHaveBeenCalled();
    expect(mockService.deleteRole).not.toHaveBeenCalled();
  });
});
