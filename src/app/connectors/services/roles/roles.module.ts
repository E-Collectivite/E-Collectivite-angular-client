import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {RolesRoutingModule} from './roles-routing.module';
import {RoleService} from './shared/role.service';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleComponent } from './role/role.component';
import { RoleFormComponent } from './role-form/role-form.component';

@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RoleListComponent, RoleComponent, RoleFormComponent],
  providers: [RoleService]

})
export class RolesModule {
}
