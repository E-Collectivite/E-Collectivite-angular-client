import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {ServicesRoutingModule} from './services-routing.module';
import {ServiceListComponent} from './service-list/service-list.component';
import {ServiceService} from './shared/service.service';
import {ServiceComponent} from './service/service.component';
import {ServiceFormComponent} from './service-form/service-form.component';

@NgModule({
  imports: [
    CommonModule,
    ServicesRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [ServiceListComponent, ServiceComponent, ServiceFormComponent],
  providers: [ServiceService],
})
export class ServicesModule {
}
