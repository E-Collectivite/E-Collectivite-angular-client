import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

import {ServiceComponent} from './service.component';
import {ServiceService} from '../shared/service.service';
import {ServiceServiceMock} from '../shared/service.service.mock';

describe('ServiceComponent', () => {
  let component: ServiceComponent;
  let fixture: ComponentFixture<ServiceComponent>;
  const mock = {
    params: Observable.of({
      connector_id: 1,
      service_id: 1,
    }),
    // required: @see https://stackoverflow.com/questions/41245783/angular-testing-router-params-breaks-test-bed
    snapshot: {}
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ServiceComponent],
      providers: [
        {provide: ServiceService, useClass: ServiceServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should return a service', () => {
    const service = fixture.debugElement.queryAll(By.css('ul'));
    expect(service.length).toEqual(1);
  });

  it('should match the second service test', () => {
    const service = fixture.debugElement.queryAll(By.css('ul'));
    const serviceTextContent = service[0].nativeElement.textContent;
    expect(serviceTextContent).toContain(ServiceServiceMock.SERVICE_2.name);
    expect(serviceTextContent).toContain(ServiceServiceMock.SERVICE_2.label);
    expect(serviceTextContent).toContain(ServiceServiceMock.SERVICE_2.class);
  });
});
