import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Service} from '../shared/service.model';
import {ServiceService} from '../shared/service.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  service: Service;

  constructor(private serviceService: ServiceService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.serviceService.getService(params['connector_id'], params['service_id'])
    ).subscribe(service => this.service = service);
  }


}
