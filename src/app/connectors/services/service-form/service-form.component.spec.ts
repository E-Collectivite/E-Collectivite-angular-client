import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {RouterTestingModule} from '@angular/router/testing';

import {ServiceFormComponent} from './service-form.component';
import {ServiceService} from '../shared/service.service';
import {ServiceServiceMock} from '../shared/service.service.mock';

describe('ServiceFormComponent', () => {
  let component: ServiceFormComponent;
  let fixture: ComponentFixture<ServiceFormComponent>;
  const params = new Subject<Params>();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [ServiceFormComponent],
      providers: [
        {provide: ServiceService, useClass: ServiceServiceMock},
        {provide: ActivatedRoute, useValue: {params}},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    params.next({'connector_id': 1});
    fixture = TestBed.createComponent(ServiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.serviceForm.valid).toBeFalsy();
  });

  it('should be valid when form is filled', () => {
    const name = component.serviceForm.controls['name'];
    expect(name.valid).toBeFalsy();

    // name is required
    expect(name.errors['required']).toBeTruthy();

    name.setValue('test');
    expect(name.valid).toBeTruthy();

    const label = component.serviceForm.controls['label'];
    expect(label.valid).toBeFalsy();
    expect(label.errors['required']).toBeTruthy();
    label.setValue('TEST');
    expect(label.valid).toBeTruthy();

    expect(component.serviceForm.valid).toBeTruthy();
  });

  it('should display validation errors', async(() => {
    expect(component.serviceForm.valid).toBeFalsy();
    component.serviceForm.controls['label'].markAsDirty();
    component.onValueChanged();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.formErrors['label']).toEqual('Label is required. ');
    });
  }));

  it('should create the service', async(inject([Router], (router) => {
    spyOn(router, 'navigate');
    params.next({'connector_id': 1});
    expect(component.serviceForm.valid).toBeFalsy();
    component.serviceForm.controls['name'].setValue('test');
    component.serviceForm.controls['label'].setValue('TEST');
    expect(component.serviceForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['connectors', 1, 'services']);
  })));

  it('should pre-render the form when updating a service', () => {
    params.next({'connector_id': 1, 'service_id': 2});
    fixture.whenStable().then(() => {
      expect(component.service.name).toEqual('helios');
      expect(component.service.label).toEqual('Hélios');
      expect(component.service.class).toEqual('\\App\\Model\\Logic\\Cas\\Pastell\\Helios');
    });
  });

  it('should edit an existing service and redirect to the service list', async(inject([Router], (router) => {
    spyOn(router, 'navigate');
    params.next({'connector_id': 1, 'service_id': 2});
    component.serviceForm.controls['label'].setValue('HELIOS - test');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['connectors', 1, 'services']);
    expect(component.service.name).toEqual('helios');
    expect(component.service.label).toEqual('HELIOS - test');
  })));
});
