import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Service} from '../shared/service.model';
import {ServiceService} from '../shared/service.service';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.css']
})
export class ServiceFormComponent implements OnInit {
  connector_id: number;
  service_id: number;
  service: Service = new Service();
  serviceForm: FormGroup;
  formErrors = {
    'name': '',
    'label': '',
  };

  validationMessages = {
    'name': {
      'required': 'Name is required.',
    },
    'label': {
      'required': 'Label is required.',
    },
  };

  constructor(private serviceService: ServiceService, private fb: FormBuilder, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.connector_id = params['connector_id'];
      if (!params['service_id']) {
        return;
      }
      this.service_id = params['service_id'];
      this.serviceService.getService(this.connector_id, this.service_id)
        .subscribe(service => this.service = service);
    });
    this.buildForm();
  }

  private buildForm() {
    this.serviceForm = this.fb.group({
      'name': [this.service.name, [Validators.required]],
      'label': [this.service.label, [Validators.required]],
      'class': [this.service.class],
    });
    this.serviceForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    const form = this.serviceForm;

    for (const field of Object.keys(this.formErrors)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  submit() {
    this.service = Object.assign(this.service, this.serviceForm.value);
    if (this.service.id) {
      this.serviceService.editService(this.service).subscribe(
        result => this.router.navigate(['connectors', this.connector_id, 'services']));
    } else {
      this.serviceService.addService(this.connector_id, this.service).subscribe(
        result => this.router.navigate(['connectors', this.connector_id, 'services']));
    }
  }
}
