import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';

import {ServiceListComponent} from './service-list.component';
import {ActivatedRoute} from '@angular/router';
import {ServiceService} from '../shared/service.service';
import {ServiceServiceMock} from '../shared/service.service.mock';

describe('ServiceListComponent', () => {
  let component: ServiceListComponent;
  let fixture: ComponentFixture<ServiceListComponent>;
  const mock = {
    snapshot: {
      parent: {
        params: {
          connector_id: 1
        }
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ServiceListComponent],
      providers: [
        {provide: ServiceService, useClass: ServiceServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display services', () => {
    const services = fixture.debugElement.queryAll(By.css('tbody tr'));
    expect(services.length).toEqual(2);
  });

  it('should match first test service', () => {
    const services = fixture.debugElement.queryAll(By.css('tbody tr'));
    const service = services[0].nativeElement.textContent;
    expect(service).toContain(ServiceServiceMock.SERVICE_1.name);
    expect(service).toContain(ServiceServiceMock.SERVICE_1.label);
    expect(service).toContain(ServiceServiceMock.SERVICE_1.class);
  });

  it('should call the delete method', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(component, 'deleteService').and.callThrough();

    const mockService = fixture.debugElement.injector.get(ServiceService);
    spyOn(mockService, 'deleteService').and.callThrough();

    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = connectors[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteService).toHaveBeenCalled();
    expect(mockService.deleteService).toHaveBeenCalled();
  });

  it('should not call the service service delete method', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    spyOn(component, 'deleteService').and.callThrough();
    const mockService = fixture.debugElement.injector.get(ServiceService);
    spyOn(mockService, 'deleteService').and.callThrough();

    const connectors = fixture.debugElement.queryAll(By.css('tbody tr'));

    const buttons = connectors[0].queryAll(By.css('button'));
    const deleteButton = buttons[1];

    deleteButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.deleteService).toHaveBeenCalled();
    expect(mockService.deleteService).not.toHaveBeenCalled();
  });
});
