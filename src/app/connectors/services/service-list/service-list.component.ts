import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Service} from '../shared/service.model';
import {ServiceService} from '../shared/service.service';

@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.css']
})
export class ServiceListComponent implements OnInit {
  services: Service[];
  connectorId: number;

  constructor(private serviceService: ServiceService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.connectorId = this.route.snapshot.parent.params['connector_id'];
    this.serviceService.getServices(this.connectorId)
      .subscribe(connectors => this.services = connectors.services);
  }

  deleteService(service: Service) {
    if (confirm('Are you sure to remove ' + service.name + ' ?')) {
      this.serviceService.deleteService(service.connector_id, service.id)
        .subscribe(response => {
          // Update the current list of services
          this.services = this.services
            .filter(oldServices => oldServices.id !== service.id);
          return this;
        });
    }
  }

}
