import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ServiceListComponent} from './service-list/service-list.component';
import {ServiceComponent} from './service/service.component';
import {ServiceFormComponent} from './service-form/service-form.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ServiceListComponent,
      },
      {
        path: 'add',
        component: ServiceFormComponent
      },
      {
        path: ':service_id',
        children: [
          {
            path: '',
            component: ServiceComponent
          },
          {
            path: 'edit',
            component: ServiceFormComponent
          },
          {
            path: 'roles',
            loadChildren: 'app/connectors/services/roles/roles.module#RolesModule',
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule {
}
