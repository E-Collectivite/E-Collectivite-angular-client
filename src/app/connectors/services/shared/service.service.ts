import {Injectable} from '@angular/core';
import {RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../environments/environment';
import {AuthHttp} from 'angular2-jwt';

import {Service} from './service.model';

@Injectable()
export class ServiceService {
  private url = environment.apiUrl + '/connectors';

  constructor(private authHttp: AuthHttp) {
  }

  getServices(connector_id): Observable<{ services: Service[] }> {
    return this.authHttp.get(this.url + '/' + connector_id + '/services.json')
      .map(response => response.json());
  }

  getService(connector_id, service_id): Observable<Service> {
    return this.authHttp.get(this.url + '/' + connector_id + '/services/' + service_id + '.json')
      .map(response => response.json());
  }

  addService(connector_id, service: Service): Observable<Service> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(service);
    return this.authHttp.post(this.url + '/' + connector_id + '/services.json', body, options)
      .map(response => response.json());
  }

  editService(service: Service): Observable<Service> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const body = JSON.stringify(service);

    return this.authHttp.put(this.url + '/' + service.connector_id + '/services/' + service.id + '.json', body, options)
      .map(response => response.json());
  }

  deleteService(connector_id, service_id): Observable<Response> {
    return this.authHttp.delete(this.url + '/' + connector_id + '/services/' + service_id + '.json')
      .map(response => response.json());
  }
}
