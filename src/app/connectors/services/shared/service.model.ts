export class Service {
  id?: number;
  name: string;
  label: string;
  class?: string;
  connector_id: number;
}
