import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {ResponseOptions, Response} from '@angular/http';

import {Service} from './service.model';

export class ServiceServiceMock {
  static SERVICE_1 = {
    id: 1,
    name: 'actes',
    label: 'Actes',
    class: '\\App\\Model\\Logic\\Cas\\Pastell\\Actes',
    connector_id: 1
  };
  static SERVICE_2 = {
    id: 2,
    name: 'helios',
    label: 'Hélios',
    class: '\\App\\Model\\Logic\\Cas\\Pastell\\Helios',
    connector_id: 1
  };

  static services = [ServiceServiceMock.SERVICE_1, ServiceServiceMock.SERVICE_2];

  getServices(connector_id): Observable<{ services: Service[] }> {
    return Observable.of({services: ServiceServiceMock.services});
  }

  getService(connector_id, service_id): Observable<Service> {
    return Observable.of(ServiceServiceMock.SERVICE_2);
  }

  addService(connector_id, service: Service): Observable<Service> {
    return Observable.of(service);
  }

  editService(service: Service): Observable<Service> {
    return Observable.of(service);
  }

  deleteService(connector_id, service_id): Observable<Response> {
    return Observable.of(new Response(new ResponseOptions({status: 204})));
  }
}
