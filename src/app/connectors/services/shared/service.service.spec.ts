import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, Http, RequestMethod, ResponseOptions, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {AuthHttp} from 'angular2-jwt';

import {ServiceService} from './service.service';

const SERVICE_1 = {
  id: 1,
  name: 'actes',
  label: 'Actes',
  class: '\\App\\Model\\Logic\\Cas\\Pastell\\Actes',
  connector_id: 1
};
const SERVICE_2 = {
  id: 2,
  name: 'helios',
  label: 'Hélios',
  class: '\\App\\Model\\Logic\\Cas\\Pastell\\Helios',
  connector_id: 1
};
const SERVICE_3 = {
  id: 3,
  name: 'signature',
  label: 'Signature',
  class: '\\App\\Model\\Logic\\Cas\\Parapheur',
  connector_id: 2
};

const SERVICES = [SERVICE_1, SERVICE_2, SERVICE_3];
describe('ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ServiceService,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, requestOptions: BaseRequestOptions) => {
            return new Http(mockBackend, requestOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        {provide: AuthHttp, useExisting: Http, deps: [Http]},
      ]
    });
  });

  it('should be created', inject([ServiceService], (service: ServiceService) => {
    expect(service).toBeTruthy();
  }));

  describe('getServices()', () => {

    it('should load services 1 and 2', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;

        const expectedServices = [SERVICE_1, SERVICE_2];
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services.json');
          const options = new ResponseOptions({status: 200, body: expectedServices});
          connection.mockRespond(new Response(options));
        });

        serviceService.getServices(1).subscribe(services => {
          result = services;
        });

        tick();

        expect(result.length).toBe(expectedServices.length, 'should have expected number of services');
        expect(result).toBe(expectedServices, 'should return the service list');
      })));

    it('should return no services', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/3/services.json');
          const options = new ResponseOptions({status: 200, body: []});
          connection.mockRespond(new Response(options));
        });

        serviceService.getServices(3).subscribe(services => {
          result = services;
        });

        tick();
        expect(result.length).toBe(0, 'should have no services');
        expect(result).toEqual([], 'should return an empty list');
      })));
  });

  describe('getService()', () => {
    it('should load a service', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/2.json');
          const options = new ResponseOptions({status: 200, body: SERVICE_2});
          connection.mockRespond(new Response(options));
        });

        serviceService.getService(1, 2).subscribe(service => {
          result = service;
        });

        tick();

        expect(result).toBe(SERVICE_2, 'should return only one service');
      })));
  });

  describe('addService()', () => {

    it('should create a service', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;
        const newService = {
          name: 'actes',
          label: 'Actes',
          class: '\\App\\Model\\Logic\\Cas\\Pastell\\Actes',
          connector_id: 1
        };

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Post);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services.json');
          const options = new ResponseOptions({status: 201, body: SERVICE_1});
          connection.mockRespond(new Response(options));
        });

        serviceService.addService(1, newService).subscribe(service => {
          result = service;
        });
        tick();

        expect(result).toBe(SERVICE_1);
      })));
  });

  describe('editService()', () => {

    it('should edit a service', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;
        const updatedService = SERVICE_1;
        updatedService.label = 'ACTES - test';

        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Put);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1.json');
          const options = new ResponseOptions({status: 201, body: updatedService});
          connection.mockRespond(new Response(options));
        });

        serviceService.editService(updatedService).subscribe(service => {
          result = service;
        });
        tick();

        expect(result).toBe(updatedService);
      })));
  });

  describe('deleteService()', () => {
    it('should delete a service', inject([ServiceService, MockBackend],
      fakeAsync((serviceService: ServiceService, mockBackend: MockBackend) => {
        let result;
        mockBackend.connections.subscribe(connection => {
          expect(connection.request.method).toBe(RequestMethod.Delete);
          expect(connection.request.url).toBe('https://cake.test.adullact.org/api/v1/connectors/1/services/1.json');
          const options = new ResponseOptions({status: 204});
          connection.mockRespond(new Response(options));
        });

        serviceService.deleteService(SERVICE_1.connector_id, SERVICE_1.id).subscribe(service => {
          result = service;
        });
        tick();

        expect(result).toBe(null, 'should return nothing');
      })));
  });
});
