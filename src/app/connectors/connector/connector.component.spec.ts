import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {By} from '@angular/platform-browser';

import {ConnectorComponent} from './connector.component';
import {ConnectorService} from '../shared/connector.service';
import {ConnectorServiceMock} from '../shared/connector.service.mock';

describe('ConnectorComponent', () => {
  let component: ConnectorComponent;
  let fixture: ComponentFixture<ConnectorComponent>;
  const mock = {
    params: Observable.of({connector_id: 1})
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectorComponent],
      providers: [
        {provide: ConnectorService, useClass: ConnectorServiceMock},
        {provide: ActivatedRoute, useValue: mock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should return a connector', () => {
    const connector = fixture.debugElement.queryAll(By.css('ul'));
    expect(connector.length).toEqual(1);
  });

  it('should match the first connector test', () => {
    const connector = fixture.debugElement.queryAll(By.css('ul'));
    const connectorTextContent = connector[0].nativeElement.textContent;
    expect(connectorTextContent).toContain(ConnectorServiceMock.CONNECTOR_1.name);
    expect(connectorTextContent).toContain(ConnectorServiceMock.CONNECTOR_1.label);
    expect(connectorTextContent).toContain(ConnectorServiceMock.CONNECTOR_1.url);
  });
});
