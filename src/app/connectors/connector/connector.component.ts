import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ConnectorService} from '../shared/connector.service';
import {Connector} from '../shared/connector.model';

@Component({
  selector: 'app-connector',
  templateUrl: './connector.component.html',
  styleUrls: ['./connector.component.css']
})
export class ConnectorComponent implements OnInit {
  connector: Connector;

  constructor(private connectorService: ConnectorService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.switchMap(
      params => this.connectorService.getConnector(params['connector_id'])
    ).subscribe(connector => this.connector = connector);
  }

}
