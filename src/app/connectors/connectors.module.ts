import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {ConnectorsRoutingModule} from './connectors-routing.module';
import {ConnectorListComponent} from './connector-list/connector-list.component';
import {ConnectorService} from './shared/connector.service';
import {ConnectorFormComponent} from './connector-form/connector-form.component';
import { ConnectorComponent } from './connector/connector.component';

@NgModule({
  imports: [
    CommonModule,
    ConnectorsRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [ConnectorListComponent, ConnectorFormComponent, ConnectorComponent],
  providers: [ConnectorService]

})
export class ConnectorsModule {
}
