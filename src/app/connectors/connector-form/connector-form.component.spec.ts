import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {ConnectorFormComponent} from './connector-form.component';
import {ConnectorService} from '../shared/connector.service';
import {ConnectorServiceMock} from '../shared/connector.service.mock';

describe('ConnectorFormComponent', () => {
  let component: ConnectorFormComponent;
  let fixture: ComponentFixture<ConnectorFormComponent>;

  const params = new Subject<Params>();
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ConnectorFormComponent],
      providers: [
        {provide: ConnectorService, useClass: ConnectorServiceMock},
        {provide: Router, useValue: router},
        {provide: ActivatedRoute, useValue: {params}}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.connectorForm.valid).toBeFalsy();
  });

  it('should be valid when form is filled', () => {
    const name = component.connectorForm.controls['name'];
    expect(name.valid).toBeFalsy();

    // name is required
    expect(name.errors['required']).toBeTruthy();

    name.setValue('test');
    expect(name.valid).toBeTruthy();

    const label = component.connectorForm.controls['label'];
    expect(label.valid).toBeFalsy();
    expect(label.errors['required']).toBeTruthy();
    label.setValue('TEST');
    expect(label.valid).toBeTruthy();

    const url = component.connectorForm.controls['url'];
    expect(url.valid).toBeFalsy();
    url.setValue('https://test.example.org');

    expect(component.connectorForm.valid).toBeTruthy();
  });

  it('should display validation errors', async(() => {
    expect(component.connectorForm.valid).toBeFalsy();
    component.connectorForm.controls['label'].markAsDirty();
    component.onValueChanged();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.formErrors['label']).toEqual('Label is required. ');
    });
  }));

  it('should create the connector', () => {
    params.next({});
    expect(component.connectorForm.valid).toBeFalsy();
    component.connectorForm.controls['name'].setValue('test');
    component.connectorForm.controls['label'].setValue('TEST');
    component.connectorForm.controls['url'].setValue('https://test.example.org');
    expect(component.connectorForm.valid).toBeTruthy();

    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['connectors']);
  });

  it('should pre-render the form when updating a connector', () => {
    params.next({'connector_id': 1});
    fixture.whenStable().then(() => {
      expect(component.connector.name).toEqual('pastell');
      expect(component.connector.label).toEqual('Pastell');
      expect(component.connector.url).toEqual('https://pastell.example.org');
    });
  });

  it('should edit an existing connector and redirect to the connector list', () => {
    params.next({'connector_id': 2});
    component.connectorForm.controls['label'].setValue('Parapheur');
    fixture.detectChanges();
    // Trigger the submit function
    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['/connectors']);
    expect(component.connector.name).toEqual('parapheur');
    expect(component.connector.label).toEqual('Parapheur');
  });
});
