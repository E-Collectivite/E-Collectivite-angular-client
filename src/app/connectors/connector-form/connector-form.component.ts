import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {ConnectorService} from '../shared/connector.service';
import {Connector} from '../shared/connector.model';

@Component({
  selector: 'app-connector-form',
  templateUrl: './connector-form.component.html',
  styleUrls: ['./connector-form.component.css']
})
export class ConnectorFormComponent implements OnInit {
  id: number;
  connector: Connector = new Connector();
  connectorForm: FormGroup;
  formErrors = {
    'name': '',
    'label': '',
    'url': '',
  };

  validationMessages = {
    'name': {
      'required': 'Name is required.',
    },
    'label': {
      'required': 'Label is required.',
    },
    'url': {
      'required': 'URL is required.',
    },
  };

  constructor(private connectorService: ConnectorService, private fb: FormBuilder,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (!params['connector_id']) {
        return;
      }
      this.id = params['connector_id'];
      this.connectorService.getConnector(this.id)
        .subscribe(connector => this.connector = connector);
    });
    this.buildForm();
  }

  private buildForm() {
    this.connectorForm = this.fb.group({
      'name': [this.connector.name, [Validators.required]],
      'label': [this.connector.label, [Validators.required]],
      'url': [this.connector.url, [Validators.required]],
    });

    this.connectorForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    const form = this.connectorForm;

    for (const field of Object.keys(this.formErrors)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  submit() {
    this.connector = Object.assign(this.connector, this.connectorForm.value);
    if (this.connector.id) {
      this.connectorService.editConnector(this.connector).subscribe(
        result => this.router.navigate(['/connectors']));
    } else {
      this.connectorService.addConnector(this.connector).subscribe(
        result => this.router.navigate(['connectors']));
    }
  }
}
