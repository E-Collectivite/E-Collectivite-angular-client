export const environment = {
  production: true,
  apiUrl: 'https://ecollectivite-backend.demonstrations.adullact.org/api/v1',
  loginUrl: 'https://ecollectivite-backend.demonstrations.adullact.org/api/v1/users/login'
};
